---
title: Abril 2021
---

# Cambá CTTE Ezine Abril 2021

![Arcoiris](@img/arcoiris.png)

## Nuestros textos poéticos

> Durante las últimas jornadas de reflexión en Cambá escribimos textos poéticos inspirados en cuentos, aquí nuestra producción:


### [La enorme nada. De María Baranda](https://www.youtube.com/watch?v=IizQhnYUC20)

> Jose/Estefi/Leito/Gabi M/Dami

```
En un camino que conjuga deseos y expectativas,
las inseguridades presentes buscan siempre acompañarnos.
Con coraje debemos los desafíos atravezar, animarnos a movernos
y accionar para vencer el miedo a la incertidumbre.
```

### [El escondite de las ganas. Jonas Ribeiro](https://www.youtube.com/watch?v=gcb5t2pBmMI)

> Charlie/Marce/Bel

```
Una pluma voló sigilosa.
Huyó de una casa gris
donde ya nadie reía.
Todo apagado, igual que ayer.
El tic toc del reloj
marcaba un día más.
Por la calle flotó
buscando a quien contagiar,
El humo, el ruido,
lentamente la alejó.
Y allá, donde nacía el arcoiris
se apoyó.
En la motivación de les inquietes, de les que anhelan reir,
encontró un rinconcito
para hacer cosquillas.
Y ahí, con la primer risa,
se desató la carcajada.
```

### [El plan. Ethel Batista y Eva Mastrogiulio](https://www.youtube.com/watch?v=E_EloFUCszI)

> Flor/Jesi/Nico/Neto/Gabi G/Jesús

```
Ante la frustración,
el lobo comenzó a recordar
cuál de todos los pasos,
lo dejaron atrás.

De repente, su soledad comenzó a cuestionar.
El olor de su manada pareció añorar,
también la cueva, aquél refugio en colectivdad.

Sus ideas eran siempre iguales, carencían de color.
No como en su tribu,
donde la diferencia todo lo potenció.

Las chanchitas mientras tanto,
en la incertidumbre eligieron hermandad.
Con creatividad y pensamiento lateral,
una a una sus herramientas pusieron a jugar.
Aunque distintos caminos tomarían,
al deseo colectivo también dieron lugar.
Idearon juntas un plan, y abandonaron la seguridad.
No sólo escaparon del lobo,
también se embarcaron a soñar.

Su viaje continuará, y nuevas adversidades sortearán.
Más la experiencia vivida las guiará.

```

## Herramientas de software

- [Secret Scanner](https://github.com/deepfence/SecretScanner) - Encontrar contraseñas en contenedores, imagenes y sistema de archivos.
- [OpenDroneMap](https://www.opendronemap.org/) - Software para mapeo con drones
- [Ice](https://github.com/alibaba/ice) - Progressive Framework basado en React - [Web](https://ice.work/)
- [Blitzjs](https://blitzjs.com/) - Fullstack React Framework (Hecho on Next.js y funcionalidad  "Zero-API" abstracción de la capa de datos (elimina la necesidad de REST/GraphQL)
- [OSINT username checker](https://github.com/soxoj/maigret) - Busqueda de información de personas por usuario en múltiples sitios.
- [Zoom Escaper Web](https://zoomescaper.com/)
    - [Zoom Deleter](https://github.com/antiboredom/zoom-deleter)
    - [Zoom Escaper](https://github.com/antiboredom/zoom-escaper)


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Entrevista sobre IA² en **El tiempo no para** en Radio Ahijuna](https://ar.radiocut.fm/audiocut/tecnologia-inteligencia-artificial-al-cuadrado/)
- [Radar de tecnologías Thoughtworks vol. 24](https://assets.thoughtworks.com/assets/technology-radar-vol-24-es.pdf)
- Lanzamiento de Huayra Linux 5 y Plan Federal Juana Manso
    - [Javier Castrillo - FLISoL BA 2021](https://www.youtube.com/watch?v=1rnXwHgx1uc)
    - [Juana Manso](https://recursos.juanamanso.edu.ar/home)
    - [Huayra Version 5](https://huayra.educar.gob.ar/)
- [¿Argentina: Software libre para controlar las vacunas?](https://www.infotechnology.com/mundo-cio/covid-asi-funciona-el-software-libre-que-usa-el-gobierno-para-controlar-las-vacunas/)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Arte, política y contracultura: encuentro en el caos, con María Galindo y Paul Preciado](https://www.lavaca.org/portada/arte-politica-y-contracultura-encuentro-en-el-caos-con-maria-galindo-y-paul-preciado/)
- [Entrvista a Silvia Federici - El sexo para las mujeres ha sido siempre un trabajo](http://lobosuelto.com/entrevista-a-silvia-federici-el-sexo-para-las-mujeres-ha-sido-siempre-un-trabajo-nuria-alabao/)

### Sociales/filosóficos

- ["Viaje a la privacidad", crónica sobre una misión hacker en Buenos Aires](viaje_a_la_privacidad)
- [Patentes sobre las vacuna covid19](https://www.jacobinmag.com/2021/04/bill-gates-vaccines-intellectual-property-covid-patents)

### Tecnología

- [¿Cómo escape a node?](https://acco.io/i-escaped-node)
- [Escucha la marcha peronista en GRUB al iniciar tu compu](https://ubuntuperonista.blogspot.com/2021/03/grubinittunemarchaperonistaubuntu.html)
- Backdoor agregado y encontrado en PHP - [Historia en slashdot](https://developers.slashdot.org/story/21/03/29/2111200/phps-git-server-hacked-to-add-backdoors-to-php-source-code) y [Código y explicación](https://www.schneier.com/blog/archives/2021/04/backdoor-added-but-found-in-php.html)
- [Configurar GIT para hacer patchs y enviarlos por email](https://wiki.texto-plano.xyz/2021/04/10/git-send-email-y-git-format-patch)
- Rust en el kernel Linux - [Primer paso](https://linux.slashdot.org/story/21/03/20/0321230/rust-takes-tentative-first-step-toward-linux-kernel), [Opinión de Linus](https://www.zdnet.com/article/linus-torvalds-on-where-rust-will-fit-into-linux/)


### Otras

Vení a descargar las frustraciones de  la semana y el capitalismo cagándote a tiros con nosotres!
Xonotic es un FPS gratuito y open source disponible para linux, mac os, y windows. Descargalo de [Xonotic](https://xonotic.org), descomprimilo, y empezá a jugar. Te esperamos los viernes a las 8 de la noche, hora de Argentina (utc -3).
Server: `radio.pungas.space:47512`
Entrá al [Chat](https://chat.rebelion.digital) para saber fechas y más!
