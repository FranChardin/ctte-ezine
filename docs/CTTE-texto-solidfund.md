# Solid Fund

Cuando viajamos a inglaterra en Abril del '19, los gastos de estadía y transporte internos fueron pagados por un fondo solidario que tienen las cooperativas de UK, donde los aportantes ponen £1 por semana. Hoy el fondo tiene £107.205

<https://solidfund.coop>

## ¿Qué es?

Es un fondo común, que se gobierna democráticamente, cuya "visión" es ser una red fuerte y *resilente*, que propicie el crecimiento y el desarrollo de cooperativas de trabajo.

Su "misión" es crear y administrar el fondo común que esta formado por los aportes voluntarios de personas individuales asociados a cooperativas de trabajo, cooperativas u organizaciones que trabajan en pos de la "democracia industrial" y la propiedad colectiva.

Su propósito es financiar actividades de capacitación, fortalecimiento de lazos y de la autonomía del movimiento cooperativo, promover el conocimiento e intercambio entre distintas cooperativas y organizaciones e identificar y difundir las mejores prácticas cooperativas. También, garantizar la continuidad hacia el futuro de la cultura del cooperativismo de trabajo.

## ¿Cómo funciona?

El fondo no es una entidad comercial, una sociedad o una organización de caridad. De hecho no esta inscripto como entidad (*unincorporated*).

El dinero es administrado a través de una entidad crediticia cooperativa cuyos lineamientos éticos solo le permiten prestar a sociedades o proyectos cooperativos, de bien común, no dañinos al medio ambiente, de comercio justo, etc: <https://coopfinance.coop/about-us-2/>

### ¿Cómo se gobierna?

Tal como es en una cooperativa, cada miembro (pueden ser individuxs u organizaciones) que aporta al fondo, tiene un voto, independientemente de cuanto dinero aportó o el tiempo que es miembro.

Si bien pueden existir miembros, solamente cuando se comienza a aportar se goza de la posibilidad de votar, siempre y cuando cumplas con estos requisitos:

* Participar del espíritu de transparencia y honestidad
* Actuar en concordancia con los valores de solidaridad y equidad al momento de tratar con otrxs miembrxs.
* Respetar la gobernanza y marco de trabajo del fondo.
* Tarabajar en concordancia con las políticas del fondo que fueron decididas por sus miembrxs.
* Tener una actitud positiva y de apoyo hacia lxs otrxs miembrxs en los foros de discusión y decisión, así como para aquellos que tienen tareas ejecutivas delegadas.

  Para ser considerado miembrx con posibilidad de votar, tenés que aportar por un período determinado. Si dejaste de aportar, o lo hiciste de manera irregular, es informado y evaluado cada 6 meses. También, puede ser que dejes de cumplir alguno de los requisitos de arriba y te excluyan.

  Si bien la base de aporte es de £1 por semana / por trabajadorx (así fue como lo crearon), te invitan a que hagas otro tipo de aportes monetarios, sin que esto signifique mayor poder de decisión.

### Desembolsos

El fondo tiene como objetivo crecer año a año, por lo cual utilizan hasta el 50% de lo recaudado en el año anterior. El dinero que no desembolsan es resguardado como una *reserva común* y es invertido de manera "segura y razonable", para garantizar la autonomía y crecimiento del movimiento cooperativo hacia el futuro.

## Toma de decisiones

Para las tomas de decisiones y propuestas, utilizan [Loomio](https://loomio.org), una plataforma para gestion de grupos y tomas de decisiones. 

El proceso de crear una *propuesta* implica crear un hilo en loomio, explicar bien qué se quiere hacer (puede ser cambio de reglas, creación de subgrupos de trabajo o aplicar el fondo para algo), dar un tiempo para que se pueda debatir, y si todo va bien, generar una *propuesta* en loomio.

Si la propuesta recibe el 75% de apoyo, se hace. Tienen 4 definiciones para votar:

* acuerdo
* desacuerdo
* abstención
* bloqueo

Si el 5% o más votos son *bloqueo* la propuesta no se realiza.

## Sub-grupos de trabajo

Para realizar distintas tareas de administración, crean subgrupos de trabajo a través de Loomio. Cada grupo debe crear un documento con las reglas y terminos de referencia, siguiendo esta estructura: propósito, composición, "poderes", tipo de conformación (elegidxs, voluntarixs, etc), tiempo de duración y cómo reportarán sus acciones para que otrxs miembrxs puedan hacer seguimiento.

Los grupos actuales son: gobernanza, gestión del fondo, marketing y comunicación, educación y cultura y sitio web y tecnología.
