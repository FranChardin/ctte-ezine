# Experiencia de Ezine - Un año y medio

## Igor Stravinsky dixit

```
Mi libertad entonces consiste en moverme en un entorno delimitado que me asigne a mi mismo
para cada una de las cosas de hago. Es más, mi libertad será mayor y más significativa cuanto
más límites al campo de acción y más obstaculos tenga que sortear. Cualquier cosa que disminuya
las restricciones también hacer disminuir la fuerza. Cuanto más restricciones uno se impone,
más uno se libera de las cadenas que atán al espíritu.
```

> Extraido y traducido del libro [Poetics of Music in the form of six lessons](https://monoskop.org/images/6/64/Stravinsky_Igor_Poetics_of_Music_in_the_Form_of_Six_Lessons.pdf) de [Igor Stravinsky](https://es.wikipedia.org/wiki/%C3%8Dgor_Stravinski)

## Experiencia

Hace una año y medio aproximadamente en la cooprativa de trabajo [Cambá](https://camba.coop), donde trabjo, surgío la necesidad de contar con referencias internas para fortalecer nuestro aprendizaje.

Se trabajo bajo esta premisa con el fin principal de cuestionarnos y además encontrar fuentes, contenidos, prácticas que nos ayuden a mejorar nuesto proceso de aprendizaje.
Es claro que mucho interrogantes, como los que planteamos por ejemplo ( ¿Quienes Somos? ¿Dondé estamos? ¿Qué queremos? ¿Qué estamos buscando? ¿Cómo nos interrelacionamos?, etc.) no tienen una respuesta clara y definitiva, sino difusa y cambiante, pero que apuntala los mecanismos del autoconocimiento, el conocimiento de las otras personas y finalmente la autonomía cooperativa.

En ese camino se creo la Coordinación técnica, tecnológica y educativa, como experiencia para marcar el terreno a transitar.

Para ello decidimos construir una revista mensual electrónica con contenido afin nuestro espiritu como organización focalizando en lo técnico y los social como grandes temas.

Todos últimos días del mes, hace un año y medio, se envía un email interno haciendo referencia a la nueva edición. Todo el contenido audiovisual es propiamente generado, curado, o propuesto por personas socias de Cambá y hay mucho trabajo detrás para seleccionar determinado contenido y mostrar tal o cual cosa.

Desde nuestra óptica es muy importante compartir material que nos permita reflexionar filosoficamente sobre nuestro contexto, sobre nosotras mismas, y también complementarlo con lo que hacemos acá en conjunto, sumamos contenido técnico especifico de las tecnologias que manejamos, y además del estado del arte en algunas otras áreas.

En esta nueva etapa estamos preparados para compartir este proceso con las demás personas, cooperativas y amigas.

Algunos de los textos que más nos gustaron:

- [La salvación de lo bello](CTTE-texto-salvacion-de-lo-bello)
- [Cita a la tesis de Belé,](CTTE-text-belen-tesis)
- [Video de Dario Z](CTTE-entrevista-dario)
- [Unicorn Grocery](CTTE-texto-cooperativas-evaluacion-pares)
- [coordinación vs conducción](Coordinación-vs.-conducción)
- [Aporte de nati](CTTE-texto-devenir-planta-bruja-maquina)
- [Bifo](CTTE-Fenomenologia-del-fin)
- [Grabois](CTTE-texto-economia-popular)
- [Concepto de libertad de espinoza](CTTE-texto-spinoza)

Durante el último año también nos hicimos una encuesta para conocer la percepción de las personas que habitamos Cambá sobre la revista, para profundizar pueden ver el resultado de la misma. [Encuesta](https://camba.gitlab.io/ctte-ezine/CTTE-encuesta-ezine-2019-analisis.html)

Para finalizar invitamos a profundizar, discutir y intercambiar información que nos ayude a crecer tanto grupalmente como individualmente para construir un futuro que merezca ser vivido.
