---
title: Mayo 2020
---

# Cambá CTTE Ezine Mayo 2020

## Texto

```
Buscando ilusiones caminamos el sendero de la muerte
Encontrando tristezas por nuestras acciones
Encontrando alegrias al ver una flor
Buscando complices para auyentar la injusticia

Desempañando el vidrio que nos impide ver
en un vasto campo de miradas, con la ilusión
de ahogar el llanto agónico de nuestro malestar

Opacando el grito de mi voz "aquí dentro"
perdida en la inmensidad de ecos, con la ilusión
de fortalecer el paso de nuestro caminar

Buscando la verdad junto a justicia
Encontrando la razón de lo irracional
Encontrando respuestas en la invisibilidad
Buscando compartir en tanta mezquindad
```

Aquí la letra de canción que escribío **Josx** de un tema inédito llamado **"Una ilusión"**


## Aportes a proyectos de Software libre

## Capacitaciones

- Capacitción sobre Procesos y Flujos de Llave en mano - [Slides](CTTE-capacitacion-llave)
- Capacitación Yoga de CTTE - [Video](https://www.youtube.com/watch?v=qvkCg7sO4sk) y [Texto](CTTE-capacitacion-yoga)

## Herramientas de software

- [Card](https://github.com/jessepollak/card) - UI para formularios con Tarjeta de crédito
- [Vite](https://github.com/vuejs/vite) - Build Tool para desarrollo web ultra rápida (creado por [Evan You](https://www.linkedin.com/in/evanyou/), [Intro](https://vuejs-course.com/blog/introducing-vite-the-fastest-dev-server-ever))
- [Wharf](https://github.com/palfrey/wharf) - Interfaz grafica para Dokku
- [1vyrain](https://1vyra.in/) - Custom bios for xx30 Thinkpad


## Informes/Encuestas/Lanzamientos
- [Inkscape v1](https://inkscape.org/news/2020/05/04/introducing-inkscape-10/)
- [Npm v7](https://blog.npmjs.org/post/617484925547986944/npm-v7-series-introduction)
- [Deno v1](https://deno.land/v1) - Runtime para js y ts liderado por el creador de Node.js, [Ryan Dahl](https://en.wikipedia.org/wiki/Ryan_Dahl)
- [Gauchoide - Juego C64 de Aventuras Argento](https://www.pungas.space/gauchoide/), otros [lanzamientos de Pungas](https://www.pungas.space/releases.html)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Hackfeminismo. Cómo montar una servidora feminista con una conexión casera.](https://soundcloud.com/traficantesdesue-os/hackfeminismo-como-montar-una-servidora-feminista-con-una-conexion-casera)
- [Haciendo Amigxs con Leonor Silvestri](http://haciendoamigxs.com.ar/)
    - [Sparring filosófico feminista. Oyèrónkẹ Oyěwùmí ft. Leonor Silvestri](https://www.youtube.com/watch?v=Yfx9dw6uymc)
    - [Feminismo, veganismo y femicidios: Solanas, Federici, Silvestri, Despentes](https://www.youtube.com/watch?v=iTOg77w5uUo)

### Sociales/filosóficos

- [Ciudad Viral](https://osvaldobaigorria.com/2020/05/16/ciudad-viral/)
- [El corona virus como teatro de la verdad](http://lobosuelto.com/el-coronavirus-como-teatro-de-la-verdad-santiago-lopez-petit)
- [30 años de Internet en Argentina: ¿Cómo inicio en el país?](https://bibliotecadigital.exactas.uba.ar/download/mensula/mensula_n019.pdf)
- [¿Descarga ilegal durante la cuarentena?](https://www.telam.com.ar/notas/202004/456072-descargas-torrents-aislamiento-cuarentena-peliculas-series-freak-utorrent-argentina.html)

### Técnicos

- [Reforzar y aprender más sobre GIT](https://learngitbranching.js.org/?demo=&locale=es_AR)
- Hubs - Plataforma Social de Realidad Virtual de Mozilla [Intro](https://hubs.mozilla.com/docs/intro-hubs.html) [Código fuente](https://github.com/mozilla/hubs)
- [Referencia sobre ASCII](https://elcodigoascii.com.ar/)
- [Camino para aprender React en el 2020](https://hackernoon.com/the-2020-reactjs-developer-roadmap-8q143yan)
- [¿Cómo hacer un crucigrama html y js?](https://mitchum.blog/building-a-crossword-puzzle-generator-with-javascript/) [Código Fuente](https://github.com/mmaynar1/games/tree/master/crossword-puzzle-generator)

### Otras

Columna de Tecnología de Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
Entrevista de Neto a [Rafael Bonifaz](https://rafael.bonifaz.ec/) del [Centro de Autonomía Digital](https://autonomia.digital/)
  - [Audio](https://archive.org/details/sonambules_wahay)
  - [Wahay](https://wahay.app/) - Aplicación descentralizada, segura y fácil de usar para hacer conferencias de audio.
