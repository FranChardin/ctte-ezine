---
title: Enero 2019
---

# Cambá CTTE Ezine Enero 2019

## Aportes a proyectos de Software libre

- [Nuka-Carousel](https://github.com/FormidableLabs/nuka-carousel/pull/506)
- [Purple Plugin delta chat](https://gitlab.com/lupine/purple-plugin-delta/merge_requests/7)
- [Release ra-data-feathers v2.0.8](https://github.com/josx/ra-data-feathers/releases/tag/v2.0.8)

## Herramientas de software

> Conjuntos de herramientas de software que sirvan para comprender y/o sean consideradas interesantes para nuestra cooperativa.

### Novedades

 - [Svelte](https://svelte.technology)<br>
   Framework js para cliente
   *It is similar to JavaScript frameworks such as React, Angular, Vue, and Ractive, which all share a goal of making it easy to build slick interactive user interfaces.*
   *But there's a crucial difference: Svelte converts your app into ideal JavaScript at build time, rather than interpreting your application code at run time. This means you don't pay the performance cost of the framework's abstractions, and you don't incur a penalty when your app first loads.*

 - [Project things](https://iot.mozilla.org/)<br>
   Plataforma para IOT. Es importante los esfuerzo para la estandarización mediante [Web things API](https://iot.mozilla.org/specification/)

 - [Arduino IOT Cloud](https://blog.arduino.cc/2019/02/06/announcing-the-arduino-iot-cloud-public-beta/)<br>
   Algo para notar: Como viene haciendo ultimamente Arduino no libera software de su plataforma online

 - [Swc](https://swc-project.github.io/)<br>
   Herramienta que hace lo mismo que [Babel](https://babeljs.io/) pero por estar hecha en Rust es 16x más rápida.

### Lanzamiento de [NativeScript 5.2](https://www.nativescript.org/blog/nativescript-5.2-comes-with-official-support-for-vue)

> Ahora nativescript-vue es parte del proyecto y cuenta con soporte oficial
> Ya hemos hecho la aplicación [ns-vue-radio](https://github.com/Cambalab/ns-vue-radio) con estas herramientas

### Lanzamiento de [Ionic 4.0](https://blog.ionicframework.com/introducing-ionic-4-ionic-for-everyone/)

> Lo más importante para nosotros es el Soporte en alpha de vuejs y react
> Fecha: 21/02/19 [Ionic react beta support](https://blog.ionicframework.com/announcing-the-ionic-react-beta/)


## Encuestas

### [Developer Survey Ionic (2018)](https://ionicframework.com/survey/2018)

> Ionic es SDK libre para desarrollo de aplicaciones mobile hibridas.

Puntos clave de la encuesta:

- *Popularidad de frameworks de js:* Los tres grandes **Vue, react y Angular**
- Ionic solo soporta estable hasta el momento Angular, el soporte de Vue esta en alfa, react 2019
- Cross-platform vs Native, audiencia nativa **8%**
- *Herramientas más usadas en orden:* **Ionic, native android, React-native, jquery mobile, native ios, Xamarin, Nativescript**
- *PWA:* herramientas más usadas **Ionic/Angular, react y vue**
- *Backend:* Mysql **59%**, firebase, mongo, postgres
- *Hosting and push:* **66%** firebase
- *Serverless:* AWS lambda **(10%)**

### [State of js (2018)](https://2018.stateofjs.com/)

- *Salarios Anual en dolares:* **29.6%** **50K-100K** , **21%** **100K-200K**

#### Frontend Frameworks

Herramientas evaludas en orden de votos: *React*, *Vue.js*, *Angular*, *Preact*, *Ember*, *Polymer*, *Svelte*, *Aurelia*, *Hyperapp*, *Backbone*, *mithril*, *jquery*, etc

Una vez más en el espacio de frontend es todo para *React* y *Vue.js*. La historia de Vue es particularmente interesante para considerar: Hace dos años solo el **27%** de los encuestados habian escuchado sobre esta biblioteca/libreria (Hoy esa fracción es solo del **1.3%**). Mientras *React* sigue teniendo una porción mucho más grande del mercado, el alza meteorica de *Vue.js* no muestra señales de parar. De hecho, *Vue.js* ya paso en determinadas metricas a su rival, como en estrellas de github.

La otra parte de la historia es la caida en los últimos años de *Angular*. Mientras aún se usa mucho, tiene una taza de insactifacción del **41%**. Es díficl ver como puede repuntar en la lista del top de los frameworks.

Finalmente hay que mantener el ojo en *Svelte*. Usando una solución radicalmente nueva, ha tomado bastante interes y ha sido mencionado como opción en la categoria de otras herramientas.

#### Data Layer

Herramientas evaludas en orden de votos: *Redux*, *GraphQL*, *Apollo*, *MobX*, *Relay/Relay Modern*, *Vuex*, *ember-data*, *ngrx*, *rxjs*, *flux*, *mongodb*, *falcor*, *cerebral*, *firebase*, *pouchdb*

*Redux* es sin duda la herramientas para usada, y su satisfacción del **82%** es una muestra de cuan bien considerado esta.

Éste espacio era siendo sacudido por la movida de *GraphQL*. Los usuarios de *GraphQL* fueron de **5%** a **20%** en dos años, y su elección para cliente es *Apollo*. Agreguen a esto el hecho de que la última versión de *Apollo* hace el uso de Redux opcional, y no sería sorpresivo y el próximo año los resultados empiezan a ser bastante diferentes


#### Backend Frameworks

Herramientas evaludas en orden de votos: *Express*, *Next.js*, *Koa*, *Meteor*, *Sails*, *Feathers*, *Hapi*, *Adonis*, *.Net*, *loopback*, *Rails*

JavaScript en el server esta en un estado extraño. Mientras innumerables frameworks emergen cada año, solamente unos pocos pueden ganar momentum para competir con *Express*. Inclusive *Koa*, a veces visto como el sucesor de *Express* tiene una muy baja satisfacción (y un número de usarios mucho menor).

Una entrada intersante en este espacio es *Next.js*, que estuvo causando mucho interes últimamente. De todas formas no es comparable con framework completo como back-end Node, esta pensado para resolver el problema del server side rendering.

Es también interesante notar también cual es el rol que van a jugar tecnologías serverless como *AWS Lambda* en los próximos años. Quien sabe, la categoría back-end como la conocemos quizas sea algo del pasado.

#### Testing

Herramientas evaludas en orden de votos: *Jest*, *Mocha*, *Jasmine*, *Enzyme*, *Karma*, *Storybook*, *Ava*, *Cypress*, *Qunit*, *Tape*, *Chai*, *Testcafe*, *Protractor*, *Selenium*, *Nightwatch*, *Sinon*, *Cucumber.js*

Mientras otras partes del ecosistema javascript están lentamente estableciendose, el testing esta fragmentado. Y los desasrrolladores en líneas generales están satisfechos con las herramientas.

La encuesta confirma que *Mocha* es aún el framework de unit testing más usado con más de 10k de usuarios (es el más establecido y grande del ecosistema, y a lo que están acostumbrados los desarrolladores denode.js).

*Jest* sigue de cerca en terminos de uso pero con una satisfacción menor de sus usuarios. Igualmente la satisfacción en una de las más grandes de toda la encuesta en general.

El espectro de testing es amplio: unit testing, integration tests, end to end tests y también "visual testing" (*storybook* es segunda herramienta en satisfacción en la categoría).

El futuro del testing puede ser que incluya más soluciones para automatizar tests en los browsers, proyectos como *Cypress* o otras herramientas basadas en *Puppeteer*


#### Mobile & Desktop

Herramientas evaludas en orden de votos: *Electron*, *React Native*, *Native apps*, *Cordova*, *Ionic*, *NativeScript*, *Flutter*, *PWA*, *Weex*, *xamarin*, *Appcelerator*, *Quasar*, *Expo*

Esta categoría muestra como crece el scope de javascript más allá del browser. *React native* y *Electron* son las dos soluciones lideres para crear app móviles y de escritorio usando tecnologías web. Pero las cosas pueden cambiar: hace poco Airbnb publico articulos de explicando porque dejan *React native* para hacer sus próximos como *Native Apps*.

Como Alternativas a *react native*, esta *Weex* que te permite hacer uso del ecosistema de *vue.js*

Google tiene algunos proyectos que entraron como *Carlo* (hecho sobre *puppeteer*) o *Flutter*.

#### Otras herramientas

- Lenguajes en orden de votos: *python*, *php*, *java*, ...
- Build Tools: *webpack*, *gulp*, *grunt*, *browserify*, *rollup*, *parcel*
- Utilidades: *lodash*, *moment*, *jquery*, *underscore*, *date-fns*
- Editores: *Vs code*, *Sublime text*, *vim*, ...

### [Encuesta de sueldos openqube](https://openqube.io/encuesta-sueldos-2019.01)

- Mediana Actual salarial en Argentina: **$50.000**
- Sueldo Sysadmin: *Junior* **$30.000**, *Semi-Senior* **$43.072**, *Senior* **$55.000**
- Sueldo Developer: *Junior* **$31.782**, *Semi-Senior* **$45.000**, *Senior* **$62.000**
- Tecnologías más populares: Plataforma: *Linux* **47,68%** , Lenguajes: *Javascript* **39,43%** , Frameworks/Herramientas/Librerias: *Node.js* **20,37%**, DB: *Mysql* **37,94%** , QA: *Selenium* **9,09%** , IDEs: *VS* **29,46%**
- 62% no tiene bonos


### [Stackshare.io (2018) Top developer tools](https://stackshare.io/posts/top-developer-tools-2018)

- Herramientas del año:
  1. [Docusaurus](https://docusaurus.io/)
  1. [Haiku](https://www.haiku.ai/features/) - Lamentablemente solo es libre el core
  1. [Prisma](https://www.prisma.io)
  1. [Tensorflow](https://js.tensorflow.org/)
  1. [vuePress](https://vuepress.vuejs.org/)

- Otros interesantes en los 30 primero:
  - [Nestjs](https://nestjs.com/)
  - [React Native Paper](https://github.com/callstack/react-native-paper)
  - [GridSome](https://github.com/gridsome/gridsome)
  - [VueStoreFront](https://www.vuestorefront.io/)
  - [PrimeReact](https://www.primefaces.org/primereact/)

### [Rising stars (2018)](https://risingstars.js.org/2018/)

Estas estadísticas están basadas las estrellas de github en los últimos 12 meses
En el siguiente texto el orden de aparición significa más estrellas.

Aplicaciones a destacar en los *10 primeros de todo:* **Ant Design**, **Vue Element Admin**, **StoryBook**

#### Frontend: Vue, React, Angular

- En el listado hay varios otros proyectos, muchas basados o alternativas a react (**dva**, **preact**, **nerve**, **relay**, **gatsby**)
- Algunos otros más nuevos como **omi**, **hyperapp**
- Algunos interesante a indagar como **Svelte** y **Stencil**

#### Backend: Next (react), Nuxt (vue), Nest (angular) contrapartes en server rendering.

- Hay algunos más **Koa**, **Feathers**, **Keystone**, **Sails**, **Loopback**, **hapi**, **meteor** (en los 20 primeros)

#### Mobile: React Native, Weex, Nativescript, Quasar, Ionic

#### Testing: Jest, Ava, Mocha, Jasmine, Tape

#### Graphql: Gatsby, Prisma, Apollo

### [StackOverflow (2018)](https://insights.stackoverflow.com/survey/2018/)

Es una encuesta a desarrolladores que usan su sitio web.

Paso a mostrar algunos detalles que me parecen interesantes, igualmente hay mucha información interesante para tomar de referencia para busquedad de gente para trabajar, venta de servicios a desarrolladores, etc.

#### Género

- *Varón* **92.9%**
- *Mujer* **6.9%**
- *Non-binary, genderqueer, or gender non-conforming* **0.9%**

Al contrario de lo que dicen su medición de audiencias que lo **"No varón" es 10% aprox.**

#### Orientación sexual:

- Straight or heterosexual **93.2%**
- Bisexual or Queer **4.3%**
- Gay or Lesbian **2.4%**
- Asexual **1.9%**

#### Raza o rasgos étnicos

- White or of European descent **74.2%**
- South Asian **11.5%**
- Hispanic or Latino/Latina **6.7%**
- East Asian **5.1%**
- Middle Eastern **4.1%**
- Black or of African descent **2.8%**
- Native American, Pacific Islander, or Indigenous Australian **0.8%**

#### Experiencia y género

We find differences among developers by gender in our survey responses. For example, twice as many women than men have been coding two years or less, evidence for the shifting demographics of coding as a profession. Also, developers who identify as transgender men or women or of non-binary gender contribute to open source at higher rates (**58%** and **60%**, respectively) than developers who identify as men or women overall (**45%** and **33%**)

#### Role y género

Developers who are educators or academic researchers are about 10 times more likely to be men than women, while developers who are system admins or DevOps specialists are 25-30 times more likely to be men than women. Women have the highest representation as academics, QA developers, data scientists, and designers.

#### Edad

- Under 18 years old **2.5%**
- 18 - 24 years old **23.6%**
- 25 - 34 years old **49.2%**
- 35 - 44 years old **17.8%**
- 45 - 54 years old **5.1%**
- 55 - 64 years old **1.5%**
- 65 years or older **0.3%**

#### Frameworks, Libraries, and Tools

- Node.js **49.6%**
- Angular **36.9%**
- React **27.8%**
- .NET Core **27.2%**
- Spring **17.6%**
- Django **13.0%**
- Cordova **8.5%**
- TensorFlow **7.8%**
- Xamarin **7.4%**

#### Programming, Scripting, and Markup Language

- JavaScript **69.8%**
- HTML **68.5%**
- CSS **65.1%**
- SQL **57.0%**
- Java **45.3%**
- Bash/Shell **39.8%**
- Python **38.8%**
- C# **34.4%**
- PHP **30.7%**

#### Pago

- Mediana de sueldo anual **USD 56,835 (USD 4736.25 por mes)**
- Rango entre **40k y 90k**

### Otras

#### [Radar - Thoughtworks (Nov 2018)](https://assets.thoughtworks.com/assets/technology-radar-vol-19-es.pdf)

> Este radar de tecnología se divide en 4 partes (Técnicas, Plataformas, Herramientas, Lenguajes y frameworks) y 4 estadios (Resistir, Evaluar, Probar, Adoptar).

## Noticias, Enlaces y Textos

> Conjunto de información que sirvan para comprender y/o sean considerados interesantes para nuestra cooperativa.

### Más técnicos

- [Guía para el comprador de notebooks para usar linux](https://www.linuxjournal.com/content/linux-laptop-buyers-guide)
- [Para Medir performance web: Lighthouse](https://addyosmani.com/blog/shine-a-light-javascript-performance/)
- [Event Stream Drama](https://schneid.io/blog/event-stream-vulnerability-explained/)
- [Our Software Dependency Problem](https://research.swtch.com/deps)
- [Why Google Stores Billions of Lines of Code in a Single Repository](https://cacm.acm.org/magazines/2016/7/204032-why-google-stores-billions-of-lines-of-code-in-a-single-repository/fulltext)
- Vue [style guide](https://vuejs.org/v2/style-guide/), [patterns](https://learn-vuejs.github.io/vue-patterns/patterns/) y [cheatsheet](https://vuejs-tips.github.io/cheatsheet/)
- Node/js [Mejores prácticas](https://github.com/i0natan/nodebestpractices), [conceptos](https://github.com/leonardomso/33-js-concepts) y [30 segundos de código](https://30secondsofcode.org/), [javascript Algorithms](https://github.com/trekhleb/javascript-algorithms), [30 seconds of react](https://github.com/30-seconds/30-seconds-of-react)

### Más sociales

- ¿Qué es la [Economía popular](CTTE-texto-economia-popular)? y [Entrevista a Juan Grabois](http://autogestionrevista.com.ar/index.php/2019/01/20/juan-grabois-la-resistencia-reside-en-la-capacidad-de-poner-en-peligro-la-estabilidad-social-y-politica/)
- Resumen de parte del libro [Fenomenología del fin](https://cajanegraeditora.com.ar/libros/fenomenologia-del-fin/) - [Franco "Bifo" Berardi](https://en.wikipedia.org/wiki/Franco_Berardi) - **[Resumen](CTTE-Fenomenologia-del-fin)**
- [La historia secreta de las mujeres programadoras](https://www.nytimes.com/2019/02/13/magazine/women-coding-computer-programming.html)
- [Stare into the lights my pretties](https://stareintothelightsmypretties.jore.cc/) - **[Resumen](CTTE-test-stare-into)**
- [Charla sobre Parecon y Pluralist Commonwealth model](https://truthout.org/articles/gar-alperovitz-and-michael-albert-a-conversation-on-economic-visions/)

## Propuestas de contribuciones a proyectos

> La idea es mensualmente publicar un conjunto de propuestas para colaborar en el tiempo libre, reutilizar en trabajo de la universidad, etc.

- Tema de Cambá para cypress: [ejemplo cypress dark](https://github.com/bahmutov/cypress-dark)
- Tareas de [Vue-admin](https://github.com/Cambalab/vue-admin)
- Generalizar [vuex-crud](https://github.com/JiriChara/vuex-crud) para que soporte múltiples clientes (al estilo que usa internamente react-admin)
- Soporte de authentication para vuex-crud
- Soporte de authorization para vuex-crud
- Traducciones de textos de la web [cultivate.coop](http://cultivate.coop), una web con mucha data de cooperativismo ( ej: [Decidiendo como decidir](http://cultivate.coop/wiki/Deciding_how_to_decide) [Resolución de conflictos en cooperativas](http://cultivate.coop/wiki/Conflict_resolution_in_co-operatives) )
