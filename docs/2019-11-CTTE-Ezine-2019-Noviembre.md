---
title: Noviembre 2019
---

# Cambá CTTE Ezine Noviembre 2019

## Frase

```
Me lo contaron y
lo olvidé; lo vi y
lo entendí; lo hice y
lo aprendí
```
Pensado y transmitido por [孔子](https://es.wikipedia.org/wiki/Confucio) que vivío hace casi 1500 años

## Aportes a proyectos de Software libre

- Lanzamiento de [Vue-admin v0.0.7](https://github.com/Cambalab/vue-admin/releases/tag/v0.0.7)

## Herramientas de software

- [RoughViz](https://github.com/jwilber/roughViz) - Creación de gráficos con visuales a mano alzada.
- [Vue camera Gestures](https://vue.cameragestures.com/) - Control para la tu app Vue usando AI con camara
- [Despacito](https://despacito-lang.github.io/updates/2019/10/28/hola-mundo.html) - Primer versión de lenguaje de programación Argento.
- [Comlink](https://github.com/GoogleChromeLabs/comlink) - Disfrutando usar Webworkers

## Informes/Encuestas

- Diagrama que hizo nuestro contador Cholakian en el pizarrón explicando la relación de `Activo = Pasivo + Patrimonio neto` - [Diagrama](CTTE-diagrama-balance.pdf)
  - [¿Cómo se hizo el diagrama?](CTTE-diagrama-balance)
- [Análisis del marco legal cooperativo Argentino](CTTE-texto-analisis-marco) - visto en lista de facttic

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Recorrido por canciones feministas](CTTE-texto-canciones-feministas)
- Escuchen a [Margarita Manterola](https://www.debian.org/women/profiles/marga), primera desarrolladora Argentina de Debian, en 2012 charlando sobre el proyecto Debian. [Audio](./assets/margarita_manterola.ogg)

### Más sociales/filosóficos

- [Charla de Mauricio Kartun sobre La creatividad](CTTE-texto-maku)
- Un libro que vale la pena leer - [Bepo, La vida secreta de un linyera](http://crotoslibres.com/HugoNario.pdf)
  Relata la peripecias de la vida de [José Américo Ghezzi](https://es.wikipedia.org/wiki/Jos%C3%A9_Am%C3%A9rico_Ghezzi) Alias ["Bepo"](http://www.crotoslibres.com/bepoghezzi.htm)

### Más técnicos

- Huevo de pascua en Basic- Página 7 de revista del [Toronto Pet user group](https://www.tpug.ca/tpug-media/nl/91-Fall2015.pdf)
  - Instalar [Vice](http://vice-emu.sourceforge.net/) - Emulador versátil de Commodore
  - Ejecutar [x64](http://vice-emu.sourceforge.net/vice_2.html#SEC6) - Emulador de Commodore 64
  - ```
    10 a=125708:gosub 20:a=33435700:gosub20: a=17059266:gosub 20:end
    20 a=rnd(-a)
    30 a=int(rnd(a)*22):if a then print chr$(a+64);:goto30 
    40 print:return
    run
    ```
- [Vinilo con software grabado](https://www.youtube.com/watch?v=ZAVuvXlbs4Q) - Juego de C64 grabado en un vinilo hace 33 años
- [Buenas prácticas para mantener proyectos grandes en vue](https://www.telerik.com/blogs/10-good-practices-building-maintaining-large-vuejs-projects)
- [Corriendo WASI en Javacript con WASMER-Js](https://medium.com/wasmer/wasmer-js-9a53e837b80)
- [Clonando la voz en 5"](https://boingboing.net/2019/11/13/this-software-can-clone-a-pers.html) - [Código Fuente](https://github.com/CorentinJ/Real-Time-Voice-Cloning)

### Seguridad

- [Monitor firefox](https://monitor.firefox.com/) o [He sido hackeado](https://haveibeenpwned.com/) - Compartido por @gm
