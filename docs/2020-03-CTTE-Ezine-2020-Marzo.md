---
title: Marzo 2020
---

# Cambá CTTE Ezine Marzo 2020

## Texto

```
Si vas a intentarlo, ve hasta el final,
de lo contrario, no empieces siquiera.
Tal vez suponga perder novixs, esposxs, familia, trabajo, y quizá, la cabeza.
Tal vez suponga no comer durante tres o cuatro días.
Tal vez suponga helarte en el banco de un parque.
Tal vez suponga la cárcel.
Tal vez suponga humillación.
Tal vez suponga desdén, aislamiento…el aislamiento es el premio.
Todo lo demás es para poner a prueba tu resistencia,
tus autenticas ganas de hacerlo.
Y lo harás, a pesar del rechazo y de las ínfimas probabilidades.
Y será mejor que cualquier cosa que pudieras imaginar.
Si vas a intentarlo, ve hasta el final.
No existe una sensación igual.
Estarás solo, con los dioses,
y las noches arderán en llamas.
Llevarás las riendas de la vida hasta la risa perfecta.
Es por lo único que vale la pena luchar.
```

Poema de [Charles Bukowski](https://es.wikipedia.org/wiki/Charles_Bukowski)


## Aportes a proyectos de Software libre

- Flor y Santi contribuyeron con el proyecto **24m** - [Frontend](https://github.com/nayracoop/24m) [Backend](https://github.com/nayracoop/24m-api)
- Nuevo versión de Fake data generator [Release 0.4.0](https://github.com/Cambalab/fake-data-generator/releases/tag/v0.4.0)

## Propuestas de contribuciones

- Generator-feathers: Al generar una conexión a la DB que aparezca como opción Mongo [issue](https://github.com/feathersjs/generator-feathers/issues/499)

## Herramientas de software

- Hooks para buscar, cachear y actualizar datos de manera asincrónica en React - [React-query](https://github.com/tannerlinsley/react-query)
- Visor de panoramas en Javascript - [PanoLens](https://pchen66.github.io/Panolens/)
- Hooks para hacer grillas de datos en React - [React-table](https://github.com/tannerlinsley/react-table)
- Evita que facebook te rastree - [Facebook container Extension](https://addons.mozilla.org/es/firefox/addon/facebook-container/)
- Mirar y controlar tu dispositivo Androide: [Scrpy](https://github.com/Genymobile/scrcpy)
- Primer validador de esquemas con inferencia de tipo para TypeScript: [ZOD](https://github.com/vriad/zod)

## Informes/Encuestas

- [Github compra npm](https://blog.npmjs.org/post/612764866888007680/next-phase-montage)


## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

-  Principales ejes y resultados de las Jornadas “Género, Ciencia, Tecnología e Innovación”, En particular hay que prestar atención al **Panel 3 - Educación STEM con enfoque de género. Marcos conceptuales, metodologías, experiencias y resultados para una integración sostenible**. [Documento](https://www.catunescomujer.org/wp-content/uploads/2016/11/Jornadas-G%C3%A9nero-Ciencia-y-Tecnolog%C3%ADa-FINAL-OK.pdf).

### Más sociales/filosóficos

- [Por un Arte impuro, Carlos Gamerro  Lecturas sobre William Borroughs](./assets/CTTE-texto-arte-impuro.pdf), en nuestra biblioteca si alguien está interesado tenemos "La revolución electrónica" de Borroughs.
- [Hacelo vos misma](CTTE-texto-diy)
- ¿Qué piensan sobre la situación actual del Corona virus algunos filosofos, pensadores? Acá algunos:
  - [Zizek](https://www.climaterra.org/post/zizek-el-coronavirus-es-un-golpe-a-lo-kill-bill-al-capitalismo)
  - [Byung-chul-han](https://www.lavaca.org/notas/byung-chul-han-sobre-coronavirus-la-emergencia-viral-y-el-estado-policial-digital-por-que-la-revolucion-sera-humana/)
  - [Judith Butler](https://www.lavaca.org/notas/el-capitalismo-tiene-sus-limites-la-mirada-de-judith-butler-sobre-el-coronavirus/)
  - [Naomi Klein](https://www.lavaca.org/notas/el-desastre-perfecto-naomi-klein-y-el-coronavirus-como-doctrina-del-shock/)
  - [Bifo](https://www.lavaca.org/notas/la-cronica-de-bifo-desde-el-confinamiento-italiano-la-epidemia-psiquica-la-paralisis-relacional-y-el-deseo-de-abrazar/)

### Más técnicos

- Divulgación: [José Edelstein](http://www-fp.usc.es/~edels/) charla sobre el [Estado de la física actual y futuro](https://www.youtube.com/watch?v=-tp3gOcNA5c)
- Memorias de un tiempo lejano: [La cultura Virii](http://0x705h.com/virii/2020/03/01/la-cultura-virii.html)
- ¿Reemplazo de Git flow? [Intro a Gitlab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
- [Awesome Coronavirus](https://github.com/soroushchehresa/awesome-coronavirus)
- Estado actual y desafios de los [Proyectos libres para construir Respiradores](https://hackernoon.com/open-source-ventilator-projects-status-challenges-how-you-can-help-j3sw3wy1)


### Otras

- [Audio Otro Universo de Comunicación](https://soundcloud.com/lavacamu/otro-universo-de-comunicaci-n)
- Recuerdo de entrevista a Cambá previo FLISOL 2019 en Programa radial Error de carga - [Audio](https://ar.radiocut.fm/audiocut/flisol2019-idea-es-personas-traigan-su-compu-y-conozcan-es-software-libre-1)


