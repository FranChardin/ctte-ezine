# Criticas al algoritmo - Revista Acción

> [Texto Completo](./assets/57-Accion.pdf)

## Algoritmos de recomendación

Algunas preguntas que se expresan, otras que surgen del texto complementadas con mi lectura.

- ¿contribuyen a enriquecer lo que los usuarios vemos y escuchamos, poniéndonos al alcance un universo casi infinito de posibilidades?
- ¿O, en cambio, su procedimiento serviría para encasillarnos, al sugerirnos un abanico limitado de opciones?
- ¿cómo repercuten las recomendaciones en la experiencia del usuario?
- ¿Uno realmente puede descubrir música que de otro modo no llegaría a conocer?
- ¿te lleva a estar rodeado únicamente de las cosas con las que te sentís cómodo?
- ¿Cuanto de la decisión final sobre esta temática esta realmente basada en la decisión del usuario?
- ¿Podemos resistirnos? ¿Es necesariamente mejor?
- ¿No estarás consumiendo lo que alguien o el algoritmo posa enfrente de tus ojos?

