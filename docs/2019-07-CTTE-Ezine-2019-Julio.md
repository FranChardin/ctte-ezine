---
title: Julio 2019
---

# Cambá CTTE Ezine Julio 2019

## Frase

```
Por medio de la absurda e improbable figura de un robot angustiado, *King* desnuda la idea de
que bajo el **temor social a ser reemplazados por máquinas**, como sugieren los más recientes
procesos de automatización industrial en el mundo, lo que se esconde es otro terror mucho más
profundo:
**el miedo a habernos convertido nosotros mismos en máquinas** ya desde hace un tiempo.
¿Acaso es tanto lo que nos diferencia de esos autómatas que repiten día tras día la misma
rutina insípida hasta que su vida útil se agota para languidecer en la obsolescencia de la
senectud?
```

Frase extraída de texto ["Los visión: más que humanos"](https://revistacrisis.com.ar/notas/los-vision-mas-que-humanos), escrita por [Luciano Rosé](https://revistacrisis.com.ar/autores/luciano-rose) en [Revista Crisis](https://revistacrisis.com.ar)


## Aportes a proyectos de Software libre

- Contribución de Jejo a [telegram-middleman-bot](https://github.com/n1try/telegram-middleman-bot/pull/8)
- Contribución de Jesús a la webapp de [software heritage](https://github.com/SoftwareHeritage), acá los cambios [D1727](https://forge.softwareheritage.org/D1727) e issue [T1858](https://forge.softwareheritage.org/T1858)
- Contribuciones múltiples de Juan a Lazyload [1](https://github.com/verlok/lazyload/pull/347) y [2](https://github.com/verlok/lazyload/pull/354). Y como si fuera poco también a los [puppeteer examples](https://github.com/GoogleChromeLabs/puppeteer-examples/pull/30)

## Herramientas de software

- Tus datos son tus datos. Construimos un sistema operativo para teléfonos inteligentes deseables, libres y con privacidad. Somos [/e/](https://e.foundation/)
- Migrando de Vscode a vim: Prueben este plugin [Vim for Visual Studio Code ](https://github.com/VSCodeVim/Vim)

## Informes/Encuestas

- Se púlica la nueva versión estable de Debian 10 aka "Buster" (25 meses de desarrollo y próximos 5 años de soporte y seguridad) [Noticia](https://www.debian.org/News/2019/20190706) <br>
  Ver [Guía de instalación](https://www.debian.org/releases/buster/amd64/) para PC 64bits.
- Se pública la [versión 2.0.0 de Reaction Commerce](https://blog.reactioncommerce.com/reaction-v2-0-0-real-time-headless-commerce-is-here/)<br>
  - Un proyecto con el cual trabajamos casi un año
  - Varios de los cambaces han contribuido con código fuente: *glmaljkovich* [24 commits](https://github.com/reactioncommerce/reaction/commits?author=glmaljkovich), *sgobotta* [1 commit](https://github.com/reactioncommerce/reaction/commits?author=sgobotta), *josx* [1 commit](https://github.com/reactioncommerce/reaction/commits?author=josx)

## Noticias, Enlaces y Textos

### Más sociales

- Inteligencia Artificial: Respeto a los derecho humanos y valores demócraticos - [Documento](CTTE-texto-ia)
- Nuestro aporte desde FACTTIC para el [Plan Nacional de Desarrollo de COOPERAR](CTTE-texto-cooperar)
- [Criticas a la sociocracia](CTTE-texto-criticas-sociocracia)
- [Dmytri Kleiner](http://www.dmytri.info/): [Venture Communism (Comunismo de Riesgo)](CTTE-texto-comunismos-riesgo)
- Entrevista de [Mario Santucho](https://revistacrisis.com.ar/autores/mario-santucho) a [Juan Grabois](https://es.m.wikipedia.org/wiki/Juan_Grabois): [La lapicera y las pasiones tristes](https://revistacrisis.com.ar/notas/la-lapicera-y-las-pasiones-tristes)
- Flor hace un resumen introductorio  la temática de "Economía Feminista" - [Texto](CTTE-texto-flor-economia-feminista)

### Más técnicos

- [Los desarrolladores no entienden CORS](https://fosterelli.co/developers-dont-understand-cors): lo que desnuda la última vulnerabilidad de **ZOOM**
- [Se empieza a vender /e/ en móviles](https://e.foundation/smartphones-with-e-ready-to-ship/)
- Mejora de Performance de componentes vue.js a partir de componentes funcionales [1](https://stegosource.com/vue-js-functional-components-what-why-and-when/) [2](https://markus.oberlehner.net/blog/working-with-functional-vue-components/)
- Lo que cada persona que programa deberia saber [Repo](https://github.com/mtdvio/every-programmer-should-know)
- [Historia de FREEDOS](https://www.linuxjournal.com/content/freedoss-linux-roots)
- [Algoritmos y estructura de datos en javascript](https://github.com/trekhleb/javascript-algorithms/blob/master/README.es-ES.md)


### Otras

- Crónica viajera de Neto en Rio cuarto - [Texto](IT10---Rio-Cuarto-062019)
- Sol nos hace escuchar [El cuarteto de nos - Contrapunto para humano y computadora](https://www.youtube.com/watch?v=C_WVQOAgll8)
- Escuchen la [Entrevista](./assets/anita_lanita.ogg) de 2011 a Anita Lanita charlando en [Oveja Electrónica](http://ovejafm.dreamhosters.com/) sobre su experiencia con el softwre libre, el amor y la permacultura.
- Estefi hizo un filtro de agua controlado por bluetooth [PSArduino](https://github.com/RoAriel/psarduino/blob/master/PSArduino.md)
- Jejo hizo un juego de batalla naval [Batalla Naval](./assets/batalla_naval.pdf)
