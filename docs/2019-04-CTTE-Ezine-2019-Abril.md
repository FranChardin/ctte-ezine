---
title: Abril 2019
---

# Cambá CTTE Ezine Abril 2019

## ¿Perfil de asociadx?

```
"Eni, repitio de año por segunda vez, oh no!"
"Eni, desocupada, tiene 20 años, padres divorciados"
Oh!, Eni vive en Zarate,
que es un pequeño infierno, pero tu miraste al cielo...

...Y descubriste algo diferente
que te hace especial para mi,
que no es tu corte moderno
o tu forma de vestir y de caminar.
Tu sabes algo mujer es lo que todos
quieren saber y averiguar...

Oh... de que color es el viento?

"Eni, repitio de año por segunda vez, oh no!"
"Eni, desocupada, tiene 20 años, padres divorciados"
Oh! Eni, vive en Zarate,
que es un pequeño infierno, pero tu miraste al cielo...

...Y descubriste algo diferente
que te hace especial para mi,
que no es tu corte moderno
tu forma de vestir y de caminar.
Tu sabes algo mujer es lo que todos
quieren saber y averiguar...

De que color es el viento?
Oh! tu no perdias el tiempo..
Oh! de que color es el viento?
Oh! tu no perdias el tiempo...

Eni, no te sientas mal! Oh no...

Oh Eni!...

Eni, tu eres especial para mi
tu eres especial para mi...
```

[Boom boom kid - Eni](https://www.youtube.com/watch?v=2dBcjuKAgBM)


## Aportes a proyectos de Software libre

- Vue-admin [Release 0.0.4](https://github.com/Cambalab/vue-admin/releases/tag/v0.0.4) - Habemus Autenticación <br>
  Increible laburo que viene haciendo *Santiago Botta* los domingos. ¡Hay que felicitarlo!
- Nuevo proyecto [Va-auth-axios-adapter](https://github.com/Cambalab/va-auth-axios-adapter) <br>
  Adaptador para externalizar la autenticación de Vue-Admin
- Aporte: Agregando dependencias a [Section-Header-Stripper](https://github.com/blackle/Section-Header-Stripper/pull/1)
- Miren la Bandera del cooperativismo programada y con ejecutable de menos de 1k. [Coop_flag](https://github.com/Cambalab/coop_flag)
- Contribución de *Santiago Botta* al proyecto [lunie](https://github.com/luniehq/lunie/pull/2406) - Es una billetera para la red Cosmos (Blockchain)
- Contribución de *José Luis Di Biase* a [purple-plugin-delta](https://gitlab.com/lupine/purple-plugin-delta/merge_requests/7)

## Herramientas de software

- [Workbox](https://github.com/googlechrome/workbox)- JavaScript Libraries for adding offline support to web apps
- [PreVue](https://github.com/teamprevue/PreVue) - Aplicación gráfica para generar código Vue
- [Deskgap](https://deskgap.com/) - Framework for building cross-platform desktop apps with web technologies (simil electron)
- [Nativescript-vue cli plugin](https://www.nativescript.org/blog/code-sharing-with-nativescript-vue) - Plugin para Generar de código para proyectos nativescript vue, inlcuido en vue cli
- [PortalVue](https://portal-vue.linusb.org/) - Un componente vue para renderizar los templates de los componentes en donde quieras.
- [Mockit](https://github.com/boyney123/mockit) - Herramienta para mockear de manera simple APIs
- [GB Studio](https://github.com/chrismaltby/gb-studio) - Creador de juegos de aventura para Gameboy (exportable a html o roms para emulador)


## Informes/Encuestas

- [State of the Vuenion](https://docs.google.com/presentation/d/1EUUu_djeNWa8kRF_uQ0DWReSpoIQn2xXCLh5A-6YdLg/edit#slide=id.p) - Evan you @ VueConfUs2019

### [IOT Developer Survey 2019](https://drive.google.com/file/d/17WEobD5Etfw5JnoKC1g4IME_XCtPNGGc/view) [1](https://eclipse-foundation.blog/2019/04/17/2019-iot-developer-survey/)

La idea de esta encuesta es entender los requerimientos, prioridades y percepciones de las comunidades de desarrollo IOT.
La encuesta la contestaron más de 1700 personas y es hecha por [Eclipse IOT working group](https://iot.eclipse.org/) de la [Fundación Eclipse](https://eclipse-foundation.blog)

Algunos resultados:

- 65% de las personas están actualmente trabajando profesionalmente en proyectos IoT o lo estará en los próximos 18 meses.
- Usan mayormente C, C++, Java, JavaScript, y Python
- AWS, Azure, y GCP son las plataformas lideres en la nube para IOT
- Las 3 aŕeas de mayor interés: Plataformas IoT , Automatización hogareña, y Automatización industrial.
- MQTT sigue siendo el protocolo dominante
- La IDE Eclipse Desktop es más usada para creación de aplicaciones IoT


### [StackOverflow (2019)](https://insights.stackoverflow.com/survey/2019/)

Es una encuesta a desarrolladores que usan su sitio web.
Extraje algunas cosas interesantes en particular por la mirada de las minorías de género y el lugar preponderante que tiene la mente y lo psicológico en el tipo de trabajo que hacemos. **¡ATENCIÓN SINDROME DEL IMPOSTOR!**

#### Estado de la discapacidad

Múltiples personas con variadas formas de discapacidad y diferencias son parte de la comunidad de desarrolladores, incluyendo a las que tienen invalidez física y problemas de salud mental. Los problemas de salud mental como la depresión y la ansiedad son particularmente comunes en las personadas encuestadas. En EEUU casi el 30% dijo haber estado con problemas de salud mental, y esa magnitud es mucho más grande que las de otros paises como UK, Canada, Alemania, o India.

#### Habilidades y experiencia

Especificámente preguntamos sobre que se evaluen en función de sus años de experiencia, y vemos una diferencia de opinión notable. Las nuevas personas que programan son menos propensas a autoevaluarse sobre la media y sigue así hasta los diez años de experiencia. Vemos aquí la evidencia del [sindrome del impostor](https://es.wikipedia.org/wiki/S%C3%ADndrome_del_impostor) entre les más juniors, patrones de duda consigo mismo generalizada, inseguridad, y miedo a ser expuesto como defraudador. Sobre las personas encuestadas los hombres crecen con más sentido de seguridad que las minorías de género.

#### Que personajes influenciaron más este año en la tecnología

Preguntamos que personas piensan que fueron las más influyentes en el 2019 (en un campo de texto vacío, sin opciones). Más allá de los CEOs de
Tesla, Amazon, Microsoft, Google, Facebook y Apple, las personas encuestadas mencionaron a desarrolladores y mantenedores de React.js y Vue.js (los frameworks preferidos de este año), también a los lideres del mundo linux y otros dominios de tecnologías. Pocos lideres mundiales han recibido mencionados y algunos individuos mencionados no estan pensados de manera favorable y/o afirmativa. Solo una mujer dentro de las 25 primeras personas de la lista: Lisa Su, la CEO de AMD.

#### Programación, Scripting, y Markup Languages

Por séptimo año consecutivo, JavaScript es el lengauaje de programación más comunmente usado, pero Python a subido de ranking nuevamente. Este año python dejo atrás a Java en el ranking, como en años anteriores dejo atrás a C# y PHP. Python es el lenguaje de programación de más rápido crecimiento en la actualidad.

#### Web frameworks

*jQuery* **48.7%**
*React.js* **31.3%**
*Angular/Angular.js* **30.7%**
*ASP.NET* **26.3%**
*Express* **19.7%**
*Spring* **16.2%**
*Vue.js* **15.2%**
*Django* **13.0%**
*Flask* **12.1%**
*Laravel* **10.5%**
*Ruby on Rails* **8.2%**
*Drupal* **3.5%**

#### Otros Frameworks, bibliotecas, y herramientas

*Node.js* **49.9%**
*.NET* **37.4%**
*.NET Core* **23.7%**
*Pandas* **12.7%**
*Unity 3D* **11.3%**
*React Native* **10.5%**
*TensorFlow* **10.3%**
*Ansible* **9.4%**
*Cordova* **7.1%**
*Xamarin* **6.5%**
*Apache Spark* **5.8%**
*Hadoop* **4.9%**
*Unreal Engine* **3.5%**
*Flutter* **3.4%**
*Torch/PyTorch* **3.3%**
*Puppet* **2.7%**
*Chef* **2.5%**
*CryEngine* **0.6%**

#### PLataformas

*Linux* **53.3%**
*Windows* **50.7%**
*Docker* **31.5%**
*Android* **27.0%**
*AWS* **26.6%**
*MacOS* **22.2%**
*Slack* **20.9%**
*Raspberry Pi* **15.2%**
*WordPress* **14.5%**
*iOS* **13.0%**
*Google Cloud Platform* **12.4%**
*Microsoft Azure* **11.9%**
*Arduino* **10.7%**
*Heroku* **10.6%**
*Kubernetes* **8.5%**
*IBM Cloud or Watson* **1.9%**


#### Developers' Primary Operating Systems

*windows* **47.5%**
*MacOS* **26.8%**
*Linux-based* **25.6%**
*BSD* **0.1%**

#### Los frameworks web preferidos, queridos y temidos

React.js y Vue.js son los dos más preferidos y queridos entre les programadores, mientras que Drupal y jQuery son los más témidos.

#### Lenagujes asociados a mayores salarios mundialmente

Globalmente las personas que usan Clojure, F#, Elixir y Rust ganan los salarios más altos, con medianas arribas de $70,000 USD. Igualmente hay variaciones regionales por ejemplo Scala en EEUU está entre los mejores pagos, mientras que Clojure y Rust son los mejores pagos en India.

#### Factor determinante en el trabajo

Preguntamos que comparen dos trabajos con el mismo pago, beneficios y locación, para que se considere alguna otra caracteristica como influencia para elegir uno u otro. Les trabajadores que forman parte de una minoría de género priorizan la cultura de la empresa y el ambiente de la oficina.

#### El mayou desafio a la productividad

La respuesta más comun incluye los ambiente de trabajo que distraen y las reuniones. Las minorías de género son menos propensos a decir que las tareas que no son de desarrollo son un problema para ellos, y más propensos a decir que son los ambientes de trabajo tóxicos.

#### Resultados generales

Python es el lenaguje de progrmación que más rápido crecío y ha subido en el ranking de nuestra encuesta nuevamente, en especial este año dejando atrás a Java y siendo el segundo lenguaje más preferido (detrás de Rust).
Más de la mitad de los encuestados ha escrito su primer línea de código cuando tenian 16 años, igualmente la experiencia varia segun país y género.
Las especialistas DevOps y ingenieras de fiabilidad (reliability) son las mejores pagas, las desarrolladoras más experimentadas estan satisfechas con sus trabajos, y tienen los niveles más bajo de busqueda de nuevos empleos.
Las desarrolladoras Chinas son las más optimistas, creyendo que la gente nacida hoy tiene una mejor vida que la de sus padres. Las desarrolladoras en los paises de europa del oeste como Franica y Alemania estan entre las que son menos optimistas sobre el futuro.
Cuando pensamos sobre que bloquea la productividad aparecen diferentes miradas según el tipo de desarrolladora. Los varones dicen más habitualmente que las tareas que son "no de desarrollo" es un problema para ellos, mientras que las minorías de género responden más habitualmente que el problema son los ambiente tóxicos.
Los datos indican que Stack Overflow ahorra de 30 a 90 minutos por semana.

## Noticias, Enlaces y Textos

### Más técnicos

- [Katie Bouman](http://people.csail.mit.edu/klbouman/) lidera la creación del algoritmo que produjo la primera imágen de un agujero negro, y ademas escribiendo software libre [Eht-imaging](https://github.com/achael/eht-imaging)
- [9 Performance secrets revealed](https://slides.com/akryum/vueconfus-2019#/) - Guillaume Chau @ VueConfUs2019
- [WASI](https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/) - Estandarización de la interfaz de sistema para WebAssembly
- [Edición 25 Aniversario de Linux Journal](https://s3.us-east-2.amazonaws.com/lj-podcast/free-issue/LinuxJournal297-April2019-PDF.pdf) <br>
  Dos Entrevistas a Linus Torvalds (Una actual y otra 25 años atrás)
- [Distro Gnu/linux Musix](https://www.fsf.org/blogs/licensing/about-musixs-removal-from-our-list-of-endorsed-distributions) pasa a ser una distro histórica de la FSF.
- [Lazy web image loading](https://addyosmani.com/blog/lazy-loading/)
- [Frontend developer handbook](https://frontendmasters.com/books/front-end-handbook/2019/) - Damian @ mattermost
- [Soporte Beta de Vue para IONIC](https://blog.ionicframework.com/announcing-the-ionic-vue-beta/) <br>
  Muy buena noticia para las personas que usan Vue como nosotres.
- [Nueva versión de UBUNTU](https://launchpad.net/ubuntu/disco) versión 19.04 llamada Disco Dingo


### Más sociales

- Amigo de la casa Rafael Bonifaz defendío su tesis ["Comunicaciones secretas en internet"](https://rafael.bonifaz.ec/blog/2019/03/comunicaciones-secretas-en-internet/)
- [Coordinación vs. Conducción](Coordinaci%C3%B3n-vs.-conducci%C3%B3n)
- Entrevista a [Luis Aranosky](https://es.wikipedia.org/wiki/Triciclosclos) - [Resumen](CTTE-Video-Luis-Aranosky)
- [La realidad de lo virtual](https://www.youtube.com/watch?v=aa07X41U37c) - [Slavoj Zizek](https://es.wikipedia.org/wiki/Slavoj_%C5%BDi%C5%BEek)

### Eventos

- [LibrePlanet 2019](https://libreplanet.org/2019/program/) - La conferencia de software libre que lleva a cabo la [FSF](https://www.fsf.org) <br>
  Se hizo el 23 y 24 de marzo
  Hubo varias charlas interesantes: *Charla Hackerspace Rancho Electrónico Martha Esperilla, Stefanía Acevedo*
  Participo como cierre del evento Micky Metts [*How can we prevent the Orwellian 1984 digital world?*](https://agaric.coop/blog/resources-preventing-digital-world-nineteen-eighty-four), que es de [Agaric Design Collective](https://agaric.coop/)
- [FLISOL](https://flisol.info/) Estuvimos en [Sastre, Santa Fé](https://flisol.info/FLISOL2019/Argentina/Sastre) y [Berazategui, Buenos Aires](https://flisol.info/FLISOL2019/Argentina/Berazategui) <br>
  Fuerte aplauso a los que pusieron el cuerpo y el tiempo para que todo funcione de la mejor manera.
  En especial a los viajeros con destino [Sastre](https://www.openstreetmap.org/search?query=sastre%2C%20santa%20fe#map=15/-31.7755/-61.8067) (Flor y Santi), y a Charlie que fue nuestro maestro organizador.
- Algunas [Crónicas de Neto](Pedir) sobre lo que va viviendo en UK

## Otros

- [Casal Catalunya Baires Open 2019](http://www.rubik.cat/OpenBaires2019) - 3 y 4 de Mayo de 2019
- [Deauther](https://www.instructables.com/id/Credit-Card-Sized-Deauther-With-Oled-Screen/) - Dispositivo para probar error en redes 802.11 (Lo que haría es desconectar y no dejar conectar a todos los que estaban conectados en la red wifi, durante el ataque)
- [Vscodium](https://itsfoss.com/vscodium/) - Si usas Visual Code, usa este fork sin código para telemetría.


## Propuestas de contribuciones

- [Buenos primeros issues para contribuir a Vue-admin](https://github.com/Cambalab/vue-admin/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22)
