# Fenomenología del fin - Franco "Bifo" Berardi

## Prólogo: ¿El fin de que?

Más allá de los usos sociales y los objetivos económicos de la tecnología en si misma:
¿Qué tipo de mutación se genera a partir de la implementación de la tecnología digital en la vida cotidiana?
Este interrogante, se dirigía escencialmente, a las variaciones que se producen a nivel de la cognición, la percepción y la sensibilidad por el hecho de habitar en un entorno digital la mayor parte de nuestras vidas.

Desplazamiento de la *conjunción* a la *conexión*

## Introducción: Concatenación, conjunción y conexión

Este libro trata sobre los efectos que produce este desplazamiento en el ámbito de la sensibilidad estéctica y de la sensitividad emocional (patrones cognitivos, comportamientos sociales, expectativas psicológicas, etc).

### Conjunción

Una concatenación conjuntiva no implica diseño original que deba ser restaurado. La conjunción es un acto creativo.
La concatenación conjuntiva es una fuente de singularidad: se trata de un evento, no de una estructura; y es irrepetible.
La conjunción es el placer de volverse otro y la aventura del conocimiento nace de ese placer.
¿Cómo sucede que, bajo ciertas circunstancias, los signos conjugados dan a luz un significado?

Paolo Virno afirma que el lenguaje, en lugar de facilitar el contacto humano, es, en realidad la fuente básica del conflicto, los malentendidos y la violencia. En la misma línea William Borroughs en la "Revolución electrónica" concibe el lenguaje como un virus que se expande creando una mutación en el entorno humano. Virno agrega que ese virus es la negación.

La conjunción de la empatía y las comprensión.

### Conexión

Llamo conexión al tipo de entendimiento que no está basado en una interpretación empática del sentido de los signos e intenciones que vienen de otro, sino, más bien, en la conformidad y adaptación a una estructura sintáctica.


### Conjunción Vs Conexión y su lógica

La conjunción puede ser vista como una manera de volverse otro, el modo conectivo de concatenación cada elemento permanece diferenciado e interactúa únicamente de manera funcional.

La conjunción requiere un criterio semántico de interpretación, la conexión require únicamente un criterio sintáctico de interpretación.

En la esfera conjuntiva estamos intentando descubrir qué es lo relevante, en la esfera conectiva partimos de un punto común de conocimiento convencional (estandares tecnológicos y formatos que lo hacen posible).

### Evolución y sensibilidad

Mi atención se centrará entonces en la modelización biosocial de la sensibilidad, es decir, en la inserción de automatismos cognitivos en los profundos niveles de la percepción, la imaginación y el deseo.

En las actuales condiciones de hipercomplejidad y de aceleración tecnogógica, la esfera social ya no puede entenderse adecuadamente en términos de intencionalidad y de transformación política.

La falta de efectividad en la acción política se debe escencialmente a un cambio en la temporalidad (ya no se puede procesar ni decidir en el tiempo).

## Sensibilidad

### La infosfera sensitiva

La sensibilidad le permite al organismo procesar signos y estimulos semióticos que no pueden ser verbalizados o codificados verbalmente.

Lo sensorial es la facultad perceptiva del organismo, mientras que lo sensual es la atracción/repulsión selectiva que el organismo proyecto sobre su entorno.

La acelaración moderna en la transmisión de signos y la proliferación de fuentes de información han transformado la percepción del tiempo.

La aceleración de la infoesfera está arrastrando la sensibilidad al vértigo de la estimulación simulada.

El inconsciente social reacciona a esta continua desterrirorialización de diversas maneras, a través de la adaptación, la desconexión o la patología.

¿Entonces que sucede? A medida que el universo electrónico de transmisores interactúa con el mundo orgánico de receptores, produce efectos patógicos: pánico, sobrexcitación, hiperactividad, trastorno por déficit de atención, dislexia, sobrecarga de información y saturación de los circuitos neuronales. La expansión del ciberespacio implica una aceleración del cibertiempo, que tiene efectos patológicos en la terminal viviente.

Producir y trabajar implican estar conectados. La conexión es trabajo. La obsesión económica provoca na permanente movilización de la energía productiva. Según Jonathan Crary, "esta es la forma de progreso contemporáneo: la implacable apropiación y dominio del tiempo y de la experiencia.

Este es el objetivo principal de la semiocorporación. Su misión es establecer una relación flexible y dinámica entre la red y el enternauta, entre la máquina y el trabajador cognitivo: Google.

La creciente demanda de atención conduce aun asedio permanente y a una expansión incesante del tiempo que se nos exige estar en alerta. La atención se ha convertidoen el recurso más escaso: puesto que no tenemos tiempo para la atención consciente, tenemos que tratar la informacíon y tomar decisiones de una manera cada vez más automática. A medida que se expande la cantidad de información que demanda nuestra atención, disminuye el tiempoel tiempo de atención disponible para su elaboración. La aceleración esta asimismo empobreciendo la experiencia, conduce a una sensibilidad reducida. La estimulación intensificada trastorna la elaboración emocional de significado.

Ocultar la pereza aparentando estar en actividad o navegar en facebook durante las horas de trabajo son parte de los patrones de comportamiento aceptados en una comunidad de trabajo. Sin embargo, estar sentado en silencio, inmóvil frente a un escritorio vacío, pensando, sonriendo y mirando la pared amenaza la paz de la comunidad y perturba la conentración de los otros trabajadores.

### La piel global

Cuando nos confrontamos con la infinitud cósmica nuestro sentidos son inducidos a una condición de confusión y pánico.

El dilema que opone a Leibnizy Spinoza, los padres fundadores de la filosofía moderna: la racionalidad recombinate de la divisibilidad finita de la materia (la mónada como información) versus la infinita sustancia del universo como una fuente inagotable de experiencia.

La historia de la civilización occidental puede ser vista como el lento e irreversible alejamiento de la naturaleza. La voluntad de abstracción es la expresión de la ansiedad y el miedo que invadió el contexto histórico y la condición para el surgimiento de la perfección digital.

Una experiencia fragmentada y un abanico cada vez más amplio de posibilidades que nunca se vuelven realidad. La irrealización y la repetición compulsiva son caracteristicas que suelen encontrarse en el comportamiento religioso y en el sexo pornogŕafico. El estigma de la neurosis obsesiva, la repetición de actos que están desprovistos de significado semántico y de eficiencia específica.
La imagen se separo del tacto, el acto pornográfico, es un acto de visión, ya no produce el placer sinestésico. La hipertrófia y la simulación del placer generan obsesión. La pertenencia religiosa se vuelve cada vez más importante en la vida de las poblaciones posmodernas a medida que se pierde el sentimiento de pertenencia a un territorio, a una comunidad o a una clase social.

## El cuerpo del general intellect

Los trabajadores cognitivos, el cognitariado, sufren una nueva forma de alienación, provocada por la separación de la actividad virtual de su existencia y comunicación corporal. La recomposición del general intellect como fuerza social y politica solo es posible Cuando la potencia intelectual del trabajo se reúne con el cuerpo afectivo de los trabajadores cognitivos. Este proceso de recomposición del trabajo pasa, por le lenguaje y el cuerpo.

Cuando el ingeniero se vincula al artista, produce máquinas destinadas a liberar el tiempo del trabajo y a desarrollar la máxima utilidad social. Cuando es controlado por el economista, produce maquinas para atar el tiempo humano y la inteligencia a la iteración de la máximización de la ganancia y acumulación del capital. Con el artista, su horizonte es la infinidad de la naturaleza y el lenguaje. Bajo el control del economista, su horizonte es el crecimiento económico y es en nombre de este dogma que destruye la naturaleza y obliga al lenguaje a ser compatible con el código.
