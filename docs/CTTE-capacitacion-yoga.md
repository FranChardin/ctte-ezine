# Clase de yoga chamu

## Descripción y consideraciones

Para la capacitación de hoy que le toca a la CTTE y analizando un poco la
situación actual en que nos encontramos y entendiendo que podría aportar
algo nuevo decidimos que vamos a prácticar un poco de Yoga versión chamu,
que es un nuevo estilo.

Para eso vamos a necesitar una manta, colchoneta o mat o similar estirada
(para estar sentados o parados sobre ella), estar sin calzado y mejor
descalzados. Además otra manta par usar de suplemento para sentarse.

Vamos a empezar a las 17hs y será un transmisión en vivo en nuestro canal de youtube.
La idea es ver la transmisión y seguir las pautas planteadas.

Luego de la clase cada uno saque un foto del lugar donde
práctico y envienla nuestros canales de comunicación.

¡Namaste!

## General

- Inspirar y exhalar solo con la nariz
- Columna estirada

## Partes

### [Savasana](https://www.yogateca.com/postura/savasana-postura-del-cadaver/)

> Disponemos el cuerpo y percibimos como estamos hoy, tanto corporalmente como
mentalmente. Visualizamos las ideas, las dolencias que tenemos

- Durante 5' relajamos en cuerpo


### [Saludo al sol o Surya Namaskar](https://www.yogateca.com/saludo-al-sol-o-surya-namaskar/)

> Surya NamaskarA: Hacemos una serie de 3, aprox 5'

- Rayo
- Pinza
- Media pinza
- Plancha alta y Plancha baja (chaturanga)
- [Urdhva mukha](https://www.yogateca.com/postura/urdhva-mukha-svanasana-postura-del-perro-hacia-arriba/) (cobra)
- [Adho mukha](https://www.yogateca.com/postura/adho-mukha-svanasana-postura-del-perro-hacia-abajo/) (perro)
    - Alejando hombros de orejas
    - Podes flexionar al principio
    - La postura completa es estirando piersnas
    - Apoyando las plantas de los pies
- Media pinza (saltando o caminando)
- Pinza: Exhalo y me pliego
- Rayo (Subo)
- Samasthiti

### Relajar cuello y hombros

- Sukhasana
    - Nos sentamos, manta por debajo, piernas cruzadas, rodillas relajadas
    - Columna/Espalda estirada, alargada
    - Focalizar y soltar la zona de cuello y hombros (respiración de 4 tiempos)

- Manos detrá de la nuca, dejo caer hacia adelante (espalda erguida)

- Mano en oreja del otro lado, dejo caer
- Igual anterior, mano detras de la oreja (hacia abajo y adelante)
- Igual del otro lado

- Hombros en circulo (adelante y atrás, termina abajo y atrás)
- Hombros arriba (contengo respiración) y luego abajo
- Cabeza arriba y abajo
- Cabeza derecha y izquierda
- Cabeza vueltas en circulo

- Mano estirada para ambos costados

- Juntar palmas atras y estirar (columna erguida)
- Gato (inhalar ) y Vaca (exhalar y redondear)
- Girar y agarrar manos, y estirar por arriba de la cabeza

- Esfinge (acostado boca abajo, cuello arriba y abajo)
- Cara en manos, dos mejillas
- Halasana (niño)
- Halasana estirando brazos

### [Savasana](https://www.yogateca.com/postura/savasana-postura-del-cadaver/)

> Integramos al cuerpo los beneficios de la práctica

- Durante 5'
