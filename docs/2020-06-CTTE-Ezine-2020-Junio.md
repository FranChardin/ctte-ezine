---
title: Junio 2020
---

# Cambá CTTE Ezine Junio 2020

## Canción

```
Escucha cómo ríen los que tienen el poder,
se ríen de nosotros porque no podemos ver
que estando separados no los podemos vencer,
que estando separados nada podemos hacer,
estamos enfrentados por nuestra estupidez,
discutiendo giladas que tienen un gran cartel,
nos sobra el orgullo y nos falta sensatez,
luchamos divididos y así vamos a caer.

Estamos prisioneros de la misma realidad,
malgastando energía en un debate virtual,
la mecha está prendida, vos de qué lado estás,
la derecha sigue avanzando mientras me bardeas.

No me ceba perder el tiempo atrapada en la red,
alimentando loros que viven de la Internet,
desvelados por la famita
cuando hay tantas cosas que hacer.

El mundo está jodido y se afea cada vez más,
acelerando el ritmo de su estupido final,
tenemos que organizarnos si lo queremos frenar,
la salida es colectiva pero vos no te avivas.

No me ceba perder el tiempo atrapada en la red,
alimentando loros que viven de la Internet,
desvelados por la famita
cuando hay tantas cosas que hacer.
```

Canción [Fuegoamigo](https://ranchoaparte.bandcamp.com/track/fuegoamigo) del disco [Vivac (2019)](https://ranchoaparte.bandcamp.com/album/vivac) de [ranchoaparte (banda hardcorepunk de Berisso)](https://www.facebook.com/RanchoxAparte/)


## Herramientas de software

- [Dojo.io](https://dojo.io) Progressive Framework for Modern Web Apps
- [Rome](https://github.com/romejs/rome), multiples herramietnas (Linting, bundling, compiling, testing, etc) hecha por facebook
- [Libreria para Auditar accesibilidad en Aplicación Vue.js](https://github.com/vue-a11y/vue-axe)
- [FunctionTrace: Python profiler](https://functiontrace.com/) basado en [Firefox profiler](https://profiler.firefox.com/)
-  Herramientas JS para estar alerta: [Webassembly Threads](https://github.com/WebAssembly/threads), [sharedArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer), [Native File API](https://wicg.github.io/native-file-system/)


## Informes/Encuestas/Lanzmientos

- [Experiencia de Ezine - Un año y medio](CTTE-articulo-ezine)
- King Empresa que hizo Candy Crash libera motor de juegos - [Defold](https://github.com/defold/defold)
- [Lenovo certifica linux para su gama de computadoras **thinkpad y thinkstation workstation**](https://news.lenovo.com/pressroom/press-releases/lenovo-brings-linux-certification-to-thinkpad-and-thinkstation-workstation-portfolio-easing-deployment-for-developers-data-scientists/)
- [System76 lanza sus Workstation portables con AMD-Ryzen](https://betanews.com/2020/06/11/system76-amd-ryzen-serval-ws-linux/)
- [Hace 25 años el jueves 8 de junio de 1996 en el newsgroup *comp.infosystem.www.authoring.cgi*  Rasmus Lerdorf hacía público PHP](https://groups.google.com/forum/#!msg/comp.infosystems.www.authoring.cgi/PyJ25gZ6z7A/M9FkTUVDfcwJ) -  [Infografía de los 25 años](https://www.jetbrains.com/lp/php-25/)


### [Stackoverflow 2020 Developer Survey](https://insights.stackoverflow.com/survey/2020)

> Encuesta de Stackoverflow que contestaron a 65000 personas del área en que trabajamos. Puede usarse como insumo para tener conclusiones más claras para elaborar estrategias en diversas áreas.

#### Lenguajes de programación más queridos

- **RUST 86.1%**
- **TypeScript 67.1%**
- **Python 66.7%**

#### Lenguajes de programación más poulares

- **Javacript 67.7%**
- **HTML/CSS 63.1%**
- **SQL 54.7%**
- **Python 44.1%**
- **Java 40.2%**

#### Frameworks Web más populares

- **Jquery 43.3%**
- **React 35.9%**
- **Angular 25.1%**

#### Frameworks Web más queridos

- **ASP.NET Core 70.7%**
- **React.js 68.9%**
- **Vue.js 66%**

#### Otros frameworks o librerias más queridos

- **Node.js 51.4%**
- **.NET 35.1%**
- **.NET Core 26.7%**

#### Tecnologías más pagadas

- **Perl 76k**
- **Scala 76k**
- **GO 74k**

#### Sistema operativo usado

- **Windows 45.8%**
- **MacOS 27.5%**
- **Linux-based 26.6%**

#### Educación

- **Titulo de grado 49.3%**
- **Titulo de posgrado 25.5%**

#### Genéro

- **Varón 91.5%**
- **Mujer 8%**
- **Otros 1.2%**
- **Trans género 1%**

#### Estado de la discapacidad

- **15% afirma tener algún tipo de ansiedad o desorden emocional**
- **2% afirma diferencias físicas**

#### Contrataciones

- **Trabajo Fulltime 70.9%**
- **Freelancer, Cuentapropista 8.9%**

#### Horas trabajadas por semana

- **Entre 40 y 44 horas 51.7%**
- **Entre 30 y 34 horas 3.6%**

#### Otros

- [Trabajo despues de hora](https://insights.stackoverflow.com/survey/2020#work-overtime)
- [¿Qué haces cuando esta trabado en algo?](https://insights.stackoverflow.com/survey/2020#technology-what-do-you-do-when-you-get-stuck)
- [¿Qué factores priorizan en la busqueda laboral los trabajadores?](https://insights.stackoverflow.com/survey/2020#work-most-important-job-factors)

### [Decentralized Developer 2020 report](https://medium.com/fluence-network/decentralized-web-developer-report-2020-5b41a8d86789)

> Encuesta de [Fluence](https://fluence.network/) que contestaron 631 personas. El valor de esta encuesta radica en este momento en conocer el estado del arte en éste área espécifica.

- La gran mayoría de proyectos encuestados fueron creados hace menos de 2 años, lo que confirma que el área de DWeb es inmadura y está comenzando.
- La DWeb es básciamente impulsada ideologicamente, pero aún no es entendida por el usuario común (más de 3/4 de los que contestaron la encuesta).
- La privacidad y la soberanía sobre los datos y la capacidad para adaptarse son las caractersiticas más esperadas de la DWeb.
- Los desafios técnicos más grande que no permite el desarrollo de la DWeb  son la conectividad de par a par y la inmadurez tecnológica.
- Entre los protocolos existentes en Internet, el que más precoupa es el de DNS, seguido de la capa de comunicación y HTTP.
- El ecosistema de la DWeb carece de un modelo de negocios y más de la mitad de los proyectos no tiene una forma de monetización.
- IPFS y Ethereum son por lejos las tecnologías más usadas para la creación de aplicaciones Dweb.
- El interes entre les desarrolladores DWeb es alto, pero el cmaino para su adopción es muy cuesta arriba por las necesidades de infraestrctura y porque hay que educar a los usuarios en relación a las ventajas en relación a la web centralizada.
- Algunas de las tecnologías mencionadas que sirven para construir este tipo de aplicaciones:
    - Protocolos de comunicación P2P: [Matrix](https://matrix.org/), [SSB](https://ssbc.github.io/scuttlebutt-protocol-guide/), [ActivityPUB](https://activitypub.rocks/), [libp2p](https://libp2p.io/)
    - Almacenamiento con Contenido direccionable: [Dat](https://dat.foundation/), [IPFS](https://ipfs.io/)
    - DNS Descentarlizado: [ENS](https://ens.domains/), [HandShake](https://handshake.org/)
    - Indentidad descentralizada: [DID](https://w3c.github.io/did-core/), [3Box](https://3box.io/)
    - Protocolos para la propiedad de datos: [Solid](https://solid.mit.edu/)
    - Redes Mesh: [Yggdrasil](https://yggdrasil-network.github.io/), [Cjdns](https://github.com/cjdelisle/cjdns/)
    - Bases de datos P2P: [Gun](https://gun.eco/), [Orbit](https://github.com/orbitdb/orbit-db)
    - Protocolos de sincronización de datos: [Braid](https://braid.news/)
    - CRDTs: [Automerge](https://github.com/automerge/automerge)
    - Control de accesos descentralizado: [Nucypher](https://github.com/nucypher/nucypher)
    - Colaboración en programación: [Radicle](https://radicle.xyz/)


### [Thoughtworks - Radar de Tecnología](https://thoughtworks.com/es/radar)
> Una guía con opiniones sobre las tecnologías de vanguardia

- [Número 22](https://assets.thoughtworks.com/assets/technology-radar-vol-22-es.pdf)


# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo
- [Esta es la 4ta entrega de la saga Como atacar realidades - Arte político para quienes odiamos la política clásica](https://www.youtube.com/watch?v=7QT7Z_Us-Qc)
- [Sci-Hub](https://sci-hub.tw/) - Repositorio de papers cientificos para que les cientificos no tengan costo de compra
    -  Creadora [Alexandra Elbakyan](https://es.wikipedia.org/wiki/Alexandra_Elbakyan) - [Sci-hub](https://es.wikipedia.org/wiki/Sci-hub)

### Sociales/filosóficos

- [Extractos del El hombre rebelde](CTTE-texto-el-hombre-rebelde)
- Revistas hipertextos, UNLP Capitalismo, técnica y sociedad en debate - [Momentos para repensar la tecnología Pablo A. Vannini de Gcoop](https://revistas.unlp.edu.ar/hipertextos/article/view/10021/8747)
- Consejos de un deportista de alto rendimiento - [Luis Scola entrevistado por Alejandro Fantino](https://www.youtube.com/watch?v=WopHC__kQQs)
    - [Éxito, fracaso, lo ordinario, y lo extraordinario](https://www.youtube.com/watch?v=WopHC__kQQs&feature=youtu.be&end=848&start=531)
    - [Seguridad, convencimiento e incomodidad](https://www.youtube.com/watch?v=WopHC__kQQs&feature=youtu.be&end=2330&start=2100)
- [La economía del software libre y open source: Multinacionales, Pymes y Comunidades - Compiladores: Hernan Morero y Jorge Motta](http://estudiosociologicos.org/portal/la-economia-del-software-libre-y-open-source-multinacionales-pymes-y-comunidades/) - Algunas referencias a páginas donde se mencionan cosas conocidas:
    - **Paǵ. 47**  *Facttic - El uso y producción de FLOSS en el sector de software de la Argentina*
    - **Pág. 120** *Caso GCOOP*
    - **Pág. 130** *Caso TECSO*
    - **Pág. 133** *Una síntesis de los casos de pymes estudiados*

### Técnicos
- [¿Como acceder a los secretos de las personas que desarrollan software?](https://hackernoon.com/how-to-steal-secrets-from-developers-using-websockets-dw3p3zgk)
    - [Ebay lo esta haciendo](https://www.ghacks.net/2020/05/25/ebay-is-port-scanning-your-system-when-you-load-the-webpage/)
- El desarrollador [Leon Hillman](https://twitter.com/littlemtman) prueba [OpenPilot en GTA V](https://littlemountainman.github.io/2020/05/12/openpilot/)
    Herramientas para la conducción automa libres:
    - [Uso real en autos](https://medium.com/@comma_ai/self-driving-car-for-free-82e871fe0587)
    - [OpenPilot](https://github.com/commaai/openpilot) - Agente de manejo autonomo libre
    - [Carla](http://carla.org/) - Simulador para investigación de conducción autonoma
- [Tutorial: GoodBoy - Perro robot con arduino e impresora 3d](https://www.instructables.com/id/GoodBoy-3D-Printed-Arduino-Robot-Dog/)
- [Logpoints: Console.log debugging](https://twitter.com/dan_abramov/status/1255692247061929991?s=09)

### Otras

- Columna de Tecnología de Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
  Entrevista de Neto Licursi Sergio Loyola y Alejandro Del Brocco de la UNQ - [Audio](https://youtu.be/4dEJYkhVAzk?start=757&end=1846)

- Ponernos en movimiento: Clases en vivo a la gorra de Rosana de [Dharmakaya Yoga & Meditación](https://www.facebook.com/dharmakaya.yoga/)
    - Míercoles 19:30hs, Viernes 19:30hs y Domingo 10hs




