# Crónica "Viaje a la privacidad"
> Escrito por [José Luis Di Biase](mailto:josx@camba.coop) durante el mes de Abril de 2021

Siempre de una manera u otra estuve relacionado con personajes, temáticas o historias de la cultura "hacker", la defensa de derechos en el entorno digital y el software libre. Hoy traigo para relatar una historia en primera persona donde confluyen viaje, caminata, privacidad y acceso a tecnología. Se trata de una misión que me encomendaron que tuvo por finalidad poder ayudar a una persona a utilizar herramientas de comunicación digitales con privacidad.

Hace unas semanas me contacto un amigo del exterior que trabaja en una organización que se propone generar privacidad en las comunicaciones digitales. Su contacto fue para que haga de nexo informático con su contraparte en Buenos Aires que brinda servicios de asistencia técnica en derecho. Mi tarea en particular era asegurar que se tenga una computadora funcional y configurada con un cliente de correo electrónico  para poder enviar y recibir emails cifrados ([Thunderbird](https://www.thunderbird.net/) con [Enigmail](https://www.enigmail.net/)), un cliente de chat con Off the record (coy.im) y un gestor de contraseñas ([keePassXC](https://keepassxc.org/))y claves GPG/PGP ([GPGKeychainAccess](https://github.com/GPGTools/GPGKeychainAccess)).
El caso que lo necesita es un caso de renombre internacional donde hay un programador perseguido por diferentes estados nación de este mundo.

Es martes por la mañana, ya hace unos días, luego de congeniar agendas estipulamos la fecha y hora de reunión. Como de costumbre el día anterior verifique la dirección donde me dirijo, miro el mapa y formas de llegar, no suelo improvisar en el camino ya que prefiero aprender para mejorar mi capacidad de poder guiarme en tres dimensiones. Muchas veces me es muy molesto modificar mi planificación previa, y esto se ve potenciado también por mi voluntad de encontrar formas de no hacerlo, por ejemplo mi decisión de no utilizar telefono móvil.

Decido tomar el subterraneo de la ciudad de Buenos Aires, en particular la línea E. [La línea E es conocida por ser la que menor caudal de personas transporta habitualmente](https://www.estadisticaciudad.gob.ar/eyc/?p=46559) y ser locación de una [pelicula de ciencia ficción local de década de los 90](https://en.wikipedia.org/wiki/Moebius_(1996_film)) muy interesante basada en la [banda o cinta de Möebius](https://es.wikipedia.org/wiki/Banda_de_M%C3%B6bius).

Empecé mi recorrido desde la Estación **Independencia** hacía el oeste, subí al último vagón donde pueden viajar también las bicletas y eramos solamente 6 personas sentadas (5 personas capturados por las pantallas de sus telefonos móviles y yo escriendo en papel). Esa captura al extremo del tiempo de atención, acaso ¿es un robo sistematico o un libre albedrío?

Leo una calcomanía pegada en las ventanas arriba de los asientos que dice así: "Ante una emergencia médica o policial llamar al 103", pero a pesar de estar subestimando a las personas,  en ese momento dude si era necesario llamar.

Llegamos a las estación Av. **La plata** bajaron 4 y solo quedamos 2. En la estación que prentendia llegar el tren nunca se detuvo, más luego me di cuenta que en época de "pandemia", los servicios estaban restringidos a ciertas estaciones.

Ese error de calculos por desconocimiento me dejo a casi dos kilometros, entonces me decido a caminar por la Av. Eva Perón/ Av. del **Trabajo** hasta el lugar al que me dirijia.

Como "Edgardo Scott" dice en "Caminantes", esta misión es la marcha del peregrino, un hombre con una causa y en el centro posee la fe, "Un gigante invisible. Un Poder sobrenatural e invisible. Una gracias, un símbolo, una metáfora. El gran poder invisible, inmemorial y milagroso de los hombres." Por estos tiempos tengo una voluntad de asemejar este tipo de caminatas con las de otros caminantes de otros tiempos: como Nestor Sanchez y su Diario de Manhattan siguiendo las enseñanzas de Gurdjieff, como Werner Herzog en su diario "Del caminar sobre hielo" o Thoreau en su periplo en Walden.

Llegue finalmente a destino y nos prestamos a la instalación y configuración específica en este caso.

Para mi esta actividad es parte de un proceso de empoderar a las personas en el uso de las tecnologías digitales de manera conciente basada en ciertos principios:

- Privacidad: Un derecho consagrado por la [Carta Universal de Derechos Humanos de las Naciones Unidas](https://www.un.org/es/about-us/universal-declaration-of-human-rights) en su articulo 12
- Anonimato: En una sociedad hipervigilada debemos garantizar el anonimato
- [Software Libre](https://es.wikipedia.org/wiki/Software_libre): Es fundamental para la privacidad porque al saber cómo están hechos los sistemas se los puede auditar. Además el software libre tiene la característica de que se puede compartir libremente y de esta manera masificar su uso sin barreras económicas artificiales.
- Criptografía:  La aplicación de la criptografía es fundamental para que una conversación sea accesible solamente para las personas que participan en la misma. Las herramientas recomendadas deben hacer uso de los mejores estándares de criptografìa para poder garantizar el secreto de las comunicaciones.
- Descentralización y autonomía: Debemos priorizar sistemas que funcionen en arquitectura P2P (Peer to peer), cuando esto no sea posible, sistemas de federación de servidores, y en el caso que no se pueda federar, el software del servidor deberìa ser libre para poder tener una instancia propia.

A continuación me propongo contarles un poco lo que se hizo. Utilizamos las guías de la EFF (Electronic Frontier Foundation) ya que tiene muchas [guías interesantes para la AUTOPROTECCIÓN DIGITAL CONTRA LA VIGILANCIA](https://ssd.eff.org/es/module-categories/gu%C3%ADas-herramientas).

### Gestor de contraseñas
Primero que nada instalamos un gestor de contraseña, en este caso [KeePassXC](https://keepassxc.org/) para poder con solo el conocimiento de una contraseña rapidamente tener acceso a todas nuestras otras contraseñas que previamente vamos a tener guardadas.Si interesa podes seguir un [tutorial de como usarlo](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassxc) en tu computadora.Lo importante es que a partir de ahora todas las claves que tengas o necesites generar la podes centralizar en esta aplicación de manera más segura.

### Emails cifrados
Para poder enviar y recibir email cifrados, necesitamos varias aplicaciones GNUPG, Mozilla Thunderbird y Enigmail. GnuPG es el programa que realmente cifra y descifra el contenido de tu correo, Mozilla Thunderbird es un cliente de correo electrónico que permite leer y escribir mensajes de correo electrónico sin necesidad de utilizar un navegador, y Enigmail es un complemento para Mozilla Thunderbird que hace que todo funcione. Este proceso es minimamente diferente según el sistema operativo que tengas instalado, pero podes seguir alguno de estos tutoriales: [Gnu/linux](https://ssd.eff.org/es/module/como-usar-pgp-para-linux), [Windows](https://ssd.eff.org/es/module/como-usar-pgp-para-windows-pc), [MacOs](https://ssd.eff.org/es/module/c%C3%B3mo-usar-pgp-para-mac-os-x).

### Chat Confidencial
Para poder chatear confidencialmente con alguien es necesario utilizar un cifrado de punta a punta. Para eso se necesitan 2 aplicaciones: Un cliente de chat (Ej: Pidgin, Adium, CoyIM) y un Complemento como OTR (Off the record, fuera de registro) que es protocolo para mantener esa confidencialidad que estamos buscando. Tutoriales: [OTR en Gnu/LInux](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-otr-en-linux), [OTR en MacOs](https://ssd.eff.org/es/module/c%C3%B3mo-usar-otr-para-mac)

### Navegar internet con privacidad y de manera anonima
Para poder navegar con internet con privacidad y de manera anonima necesitas utilizar un servicio como TOR. TOR es un servicio que provee esa privacidad y te enmascara tu presencia en línea (quien sos y donde estas), además te protege de la misma red (sos anonimo para los demas usuarios de la red). Tutoriales: [TOR en Gnu/Linux](https://ssd.eff.org/es/module/como-usar-tor-en-linux), [TOR en macOS](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-tor-en-macos), [Tor en Windows](https://ssd.eff.org/es/module/c%C3%B3mo-usar-tor-en-windows)

Es importante notar que es mucho más seguro también usar software libre como sistema operativo, Gnu/Linux es una opción ineludible. Si además queres tener más protección contra la vigilación y la censura podes utilizar [Tails](https://tails.boum.org/), un sistema operativo portátil.

Invito a las personas interesadas o que tengan necesidades puntuales a hacer el intento o pedir ayuda para lograr tener un uso más privado, anónimo, autónomo y descentralizado de las tecnologías digitales.

Una vez terminado emprendo la retirada. Vuelvo al subte, línea E, subo en vagón Siemmnes AEG 36A y veo 19 personas sentadas (1 leyendo, 1 durmiendo, 1 cruzada de brazos, 1 escribiendo en papel, 15 capturados por sus celulares y solo 2 personas acompañadas). Hago dos combinaciones y vuelvo a la superficie para caminar nuevamente por la calle **Vuelta de obligado** seguiendo mi peregrinación con otra causa un poco más abstracta, pero igual de satisfactoria dispuesto a encontrar **"personas salvajes, no personas domesticadas"** fortaleciendo **"mi deseo de conocimiento que a veces es intermitente pero la de involucrar mi mente en atmósferas ignoradas por mis pies es perenne y constante."**
