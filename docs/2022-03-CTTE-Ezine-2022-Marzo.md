---
title: Marzo 2022
---

# Cambá CTTE Ezine Marzo 2022

![aguas](@img/aguas.png)

```
Somos un río calmo, cuerpito de agua que busca ir
Somos un río bravo, cuando el mal tiempo nos deja estar
Somos agüita fresca, que rega a la claridad
Si nos salimos de cauce, hacemos daño como seguir...
Si te quedas empantanado
Ya no aguantas ni tu propio olor
Acordate adonde ibas, el futuro es hoy
El futuro es hoy
Somos un río calmo, cuerpito de agua que busca ir
Somos un río bravo, cuando el mal tiempo nos deja estar
Somos agüita fresca, que rega a la claridad
Si nos salimos de cauce, hacemos daño como seguir...
Si te quedas empantanado
Ya no aguantas ni tu propio olor
Acordate adonde ibas, dale que el futuro es hoy
El futuro es hoy
Si te quedas empantanado
Ya no aguantas ni tu propio olor
El futuro es hoy
Fluir no es dejarse llevar
Fluir y en la mirada el sueño del mar
Fluir...
```

Aquí letra de canción [De agua](https://www.youtube.com/watch?v=nZGFWDXzExs) de la banda [Arbolito](https://es.wikipedia.org/wiki/Arbolito_(banda)). Su nombre es en honor a un jefe ranquel [Nicasio Maciel](https://es.wikipedia.org/wiki/Nicasio_Maciel).


## Aportes de Cambá

- [Algunas notas sobre la bi-modalidad a partir del trabajo con Batán Coop](https://ltc.camba.coop/2022/01/31/algunas-notas-sobre-la-bi-modalidad-a-partir-del-trabajo-con-batan-coop/)

## Herramientas de software

- [VDO.Ninja](https://github.com/steveseguin/vdo.ninja) - Herramienta para inyectar video remoto via WebRTC en OBS o similar.
- [LittleJs](https://github.com/KilledByAPixel/LittleJS) - El motor minúsculo de juegos en Js que se la banca.
- [Yunohost](https://yunohost.org/es) - sistema operativo libre con el objeto de simplificar la administración de servidores y democratizar el auto-hospedaje web.
- [Jless](https://jless.io/) - Visor JSON para la terminal


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Honorarios profesionales del área con Actualización Enero/22](https://coprocier.org.ar/web/?p=2734) del  [Colegio de Profesionales de Ciencias Informáticas Entre Ríos](https://coprocier.org.ar/web/)
- Recopilaciones anuales del 2021
    - [Resultados encuesta Estado de Javascript 2021](https://2021.stateofjs.com/)
    - [Estado de Webassembly 2021 de Platform UNO](https://platform.uno/blog/the-state-of-webassembly-2021-and-2022/)
    - [Ranking de herramientas para les desarroladores](https://stackshare.io/posts/top-developer-tools-2021)
- [Videos de la 6ta React Conf de 2021](https://reactjs.org/blog/2021/12/17/react-conf-2021-recap.html)
- [Cataluña lanza su software libre para escuelas como alternativa google y microsoft](
https://elpais.com/espana/catalunya/2022-02-09/barcelona-crea-un-software-para-las-escuelas-alternativo-a-google-y-a-microsoft.html)


## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [¿De dónde viene la idea de que las mujeres chismosean?](https://tintalimon.com.ar/post/de-d%C3%B3nde-viene-la-idea-de-que-las-mujeres-chismosean/)
- [Tecnologías de género xenofeminista](https://filosofiadelfuturo.com/xenofeminismo-y-tecnologia/)
- [El feminismo en el que creo](https://www.youtube.com/watch?v=4huVd-W7vGQ)
- [Centenario de la rebelión de las "Putas de San julián"](https://elgritodelsur.com.ar/2022/02/a-cien-anos-de-rebelion-putas-de-san-julian.html)


### Sociales/filosóficas

- [Paso de Adrian dárgelos por Caja Negra](https://www.youtube.com/watch?v=3Wz3Ln7YI-0)
- [¿Y la plata dónde está?](https://quema.ar/y-la-plata-donde-esta/) por Mariano Zukerfeld
- [Fiebre del oro encriptado](https://revistacrisis.com.ar/notas/fiebre-del-oro-encriptado) por Revista Crisis
- [Cómo robar el mundo: Sebastián De Caro at TEDxTandil](https://www.youtube.com/watch?v=9bI8NNoswSM)


### Tecnología

- Microsoft y el gobierno de Salta en Argentina utilizaron datos privados de miles de niñas para intentar predecir los embarazos adolescentes. Salió bastante mal.
    - [Investigación internacional por Wired](https://www.wired.com/story/argentina-algorithms-pregnancy-prediction/)
    - [Repercusión en Hipertextual](https://hipertextual.com/2022/02/el-algoritmo-que-uso-datos-privados-de-ninas-pobres-argentinas-para-predecir-sus-embarazos)
- [Las aventuras salvajes de un duo (padre/hijo) en la busqueda de bitcoins perdidos](https://www.theblockcrypto.com/post/134443/the-wild-adventures-of-a-father-son-duo-that-go-searching-for-stuck-bitcoin)
- [Algunos comentarios técnicos sobre la WEB3](https://moxie.org/2022/01/07/web3-first-impressions.html)
- [¿Como es el proceso de gestión del kernel linux?](https://www.kernel.org/doc/html/v4.12/process/management-style.html)

### Otras

- [Video ¿Como nos organizamos para lña sostenibilidad en los comunes digitales?]( https://bbb.de.meet.coop/playback/presentation/2.3/ff16731f1cbc3a25ea8332c15a0c65fb6e739b43-1643043725985) por [Femprocomuns](https://femprocomuns.coop/)
- Costa de Villa Dominico, el Agroecológico de la UST y los vecinos. [Capítulo 1](https://fb.watch/bbOEws8m5e/), [Capítulo 2](https://www.facebook.com/watch/?v=1759208130944555)
- [Red Comunitaria Gallinato](https://www.youtube.com/watch?v=AlMrJ4P8AzI)
