# Criticas a la sociocracia

> Extraído y traducido del blog de [Laird Schaub](https://communityandconsensus.blogspot.com/2014/08/critique-of-sociocracy.html) orinigalmente escrito el 18 de agosto de 2014.

En función de mi punteo de salvedades acerca de la sociocracia (gobernanza dinámica), como un sistema de gobernanza para grupos cooperativos, especialmente que dependen de la participación voluntaria es que no estoy muy emocionado sobre su funcionamiento.
En esta monografía le pongo una atención especial a los contrastes con el consenso, que lo que basicamente la sociocracia esta buscando.

## No se hace cargo de las emociones

Una de las cosas que me preocupa es que no hay ninguna mención en su articulación a como entendemos o trabajamos sobre las emociones. Yo veo esto como un comoponente escencial en las dinámicas de grupo y lo considero un error serio.

Una vez un defensor de la sociocracia me dijo que una vez que se empieza a usar, nadie de enoja. ¡Por favor! Si tenes un sistema que funciona bien si todos piensan y se comportan racionalmente entonces tenes un equilibrio inestable. Esto no es un sistema; es una parte.

## Cómites con doble enlace/relación (o "circulos" en lenguaje sociocratico)

Cuando un grupo es lo suficientemente grande (probablemente cuando pase les 12 integrantes o quizás menos) tiene sentido crear una estructura de comités para delegar tareas. Mientras que las personas pueden trabajar en más de un comité, es importante saber como se relacionan entre ellos, y a un el grupo en general.

Mientras que lo de arriba es el ABC de las estructuras organizacionales, en la sociocracia hay una vuelta de rosca para los comités que trabajan en conjunto (cuando uno supervisa el trabajo de otros, o cuando se espera que trabajen en conjunto) se le pide que tengan un representante en cada comité con el que están relacionado. Estos representantes (cada relación) sirve de enganche y enlace de comunicaciones de un comité con otro, ayudando a asegurar que los mensajes y sus matices sean transmitidos lo más preciso posible.

Mientras que esto suena bien en teoría ( y funcione bien en la práctica en los ambientes corporativos para los que la sociocracia fue creada originalmente), habitualmente cae en un problema crónico en grupos cooperativos que son altamente dependientes de los espacios a llenar por voluntarios: demasiados espacios y poca gente debe ocuparlos también. En mis 27 años de consultor de procesos para grupos cooperativos, no recuerdo haber encontrado un grupo que facilmente a podido ocupar todos los roles de los comités y gestión. La sociocracia alegremente pide que los grupos agreguen una capa de responsabilidad para lo que ya hacen, lo que significa más asignaciones a comités. No es posible de hacer.


## El proceso de selección requiere que surjan las inquietudes de los candidatos en el lugar

Una los aspectos más díficiles de las dinámicas de grupos cooperativos es como manejar las opiniones criticas. Eso incluye muchos desafios que no son para nada triviales:

- Crear una cultura en la cual las opiniones criticas en relación a lo que hace el grupo sean valorizadas y alentadas.
- Ayudar a las personas a encontrar coraje para decir cosas díficiles.
- Ayudar a las personas que dicen cosas criticas a resolverlas (y procesar separadamente) a pesar de los trastornos o reacciones que puedan generar, para no dejar en la nada las opiniones de los que se atreven a hacerlas.
- Ayudar a los receptores de las criticas a responder criticamente de manera abierta y no defensiva.

Aunque esto es muy beneficioso, ninguna de estas cosas es necesariamente fácil de hacer, y mi experiencia en el campo me enseño el valor de darleopciones a las personas en como es la mejor manera de dar y recibir opiones criticas. (para muchos es absolutamente enloquecedor poder ser criticado publicamente)

En el caso de la sociocracia, el modelo dice de seleccionar personas para ocupar los roles (como una puesto en un comite o un lugar de gestión) en un proceso todo junto en el que se llama a nominar, charlar sobre la idoneidad del candidato y hacer la decisión final.

Mientras esto es muy bueno para la eficiencia, no puede convencerme de que esto promociona una puesta común lo más honesta posible, el procesar de las criticas, o considerar reflexivamente sobre los candidatos malos. Puedo imaginar que esta aproximación puede funcionar bien en un grupo totalmente maduro, individuos concientes, ¿Cuantos grupos como ese conoces? Yo tampoco.

## Los conceptos de preocupaciones "extremas" y "consentimiento" frente a "consenso"

La sociocracia caracteriza a los participantes solo expresando:
- Las preferencias sobre que debería ser tomado en cuenta
- El ruido que generan ciertas propuestas, que constituyen una preocupación "extrema"

El termino "extremo" no esta definido y resulta en confusiones sobre cual es el estandar que representa. Yo creo que esto se acerca al principio básico de consenso y se hace eco de la las distinciones entre las preferencias personales y las grupales, y deberia expresarse cuando es muy movilizador. No encuentro que este principio ilumine o sea diferente del consenso.

Las segunda parte confusa es la retorica es insistir que la sociocracia busca "consentimiento" más que el "consenso". Creo que se trata de fogonear la atmosfera de "es lo suficientemente bueno" en contraste de "es perfecto".

Para estar seguro, hay ansiedad en el consenso, a ser tomado como rehen por una obstinada minoria que no desea que una propuesta prospere porque ven puede haber resultados negativos y tienen miedo de quedar atrapados. Esto conduce a la parálisis. Mientras no deberia ser díficl cambiar un acuerdo no efectivo (una vez que la experiencia de su aplicación ha sido expuesta como débil), Yo creo en una mejor forma de manejar la dinamica de la tiranía de la minorias y es educando a los participantes (entrenamiento sobre la lectura del consenso) y desarrollando culturas de una confianza muy grande caracterizada por una buena escuchay popuestas de desarrollo que toman en cuenta todas las visiones.

Finalmente, el consentimiento de la sociocracia no es significativamente del consenso, es solamente un juego de palabaras.

## Las circulos no son siempre el mejor formato

La sociocracia esta enamorada de los circulos, donde cada uno tiene una forma de dar comentarios sobre temática directa. Mientras que es importante mantender la oportunidad de opinar, esto es solamente una de muchas formas disponibles de como pedir o solicitar opinion sobre algún tema (otros incluyen la discusuión abierta, compartir circulos, escritos individuales, pequeños geupos, silencio, visualización guidada, peceras. Cada una tiene su proposito, y tiene sus ventajas y responsabilidades.

Mientras los circulos son muy buenos protegiendo el tiempo para charlar para esos que son timidos (sobre las discusiones abiertas) y sirven como un bozal efectivo para aquellos que toman más tiempo en charlar, tienen la tendencia a ser lentas y repetitivas. Si las aceleras (rondas/circulos relámapgos) esto pega de lleno sobre el uso del tiempo, pero a expensas de engañar a los que naturalmente tardan más en reflexionar, y están listos para charlar o a los que encuentran charlar en grupo intimidantes.

Si solamente tenes una herramienta (el martillo), rápidamente todo empieza a parece un clavo y la realidad no es unidimensional y ¿quien quiere acostarse en una cama de clavos? Necesitas más herramientas que solo una.

## Comenzando con propuestas

En la sociocracia (y en muchos grupos usando el consenso) se espera que cuando una petición llega a la asamblea/plenario, llegue en forma de propuesta ("Este es el problema y esta es la forma sugeridad de solución"). De hecho, no hay tiempo en la agenda del plenario a no ser que haya una propuesta.

Mientras esto fuerza a  que este listo para el plenario (algo bueno) y a veces ahorra tiempo (cuando la propuesta es excelente y hace un buen trabajo anticipandose a las necesidades a tomar el cuenta y balancear los factores en pugna), también puede ser un problema. En mi experiencia es mejor que si algo es realmente importante para que el plenario tome en cuenta, que no se haga una propuesta hasta acordar que es necesario hacerlo realmente y evaluar su peso relativo. Si los gestores o comites piensan en esto último (y pueden hacer tiempo en su agenda), quizás tengan que invertir tiempo en una solución que termine en la basura.

No solamente es desmoralizante que los que generan las propuesta, sino que hace dar un giro a la conversación en como responder a la problemática ("¿Qué es lo que hay que tener en cuenta para encarar este problema?") es una pregunta diferente que "¿Es esta propuesta adecuada para encarar nuestra preocupación/problema?". En escencia, liderando la propuesta es poner el carro (la solución) adelante de los caballos (lo que la solución necesita balancear)

Los grupos cooperativos se equivocan mucho con esto, y la sociocarcia también cae en esto.
