---
title: Noviembre 2023
---

# Cambá CTTE Ezine Noviembre 2023

<div style="text-align: center;">

![Ilus](@img/noviembre2023.png)

</div>

```
Si la rutina te asesina la razón proba
salir a caminar un rato bajo el sol
podrias poner un disco de los Ramones
y acompañar con tu guitarra She´s the one

Y olvidarte los problemas
mañana es una hora nueva
no pensar en lo que no se puede cambiar

No importa si es Martes Jueves Lunes
cada dia es un dia mas
y lo malo siempre puede cambiar

Lo malo siempre puede cambiar

No importa si es Martes Jueves Lunes
cada dia es un dia mas
y lo malo siempre puede cambiar
```

Desde Lanús Oeste: letra del tema [Martes, Jueves, Lunes](https://www.youtube.com/watch?v=WoqWBTfBkAw) de las ["Las Vin Up"](https://www.instagram.com/lasvinup/)

## Cooperativas/Economía Popular
- [Crónica JRSL 2023](https://blog.camba.coop/jornadas-regionales-de-software-libre-2023/)
- [¡Leyendas TECSEANAS! Cecilia Beccaria - TECSO](https://www.youtube.com/watch?v=9rSE-YTs58Q)
- [Tutorial - Consejo de administración](https://www.youtube.com/watch?v=YeyslHROJjw)

## Herramientas de software
- [Protomaps](https://protomaps.com/): Sistema libre para interactuar con mapas web.
- [Bypass Paywalls](https://github.com/iamadamdev/bypass-paywalls-chrome): Extensión para Chrome y Firefox para saltear pantallas de pago (ej diarios).
- [Wasp.dev](https://wasp-lang.dev/): Similar a Rails framework para React, Node.js y Prisma.
- [KitForStartUps](https://github.com/okupter/kitforstartups): SvelteKit SaaS boilerplate
- [BankToken](https://github.com/nicolas17/banktoken): Script para activar tokens de Banelco en otras aplicaciones de 2FA

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Bestofjs](https://bestofjs.org/): Un lugar donde encontrar proyectos libe para la web
- Resultados de [js13k 2023](https://github.blog/2023-10-13-js13k-2023-winners/): Competencia que anual que desafia a les participantes a crear jueves en 13kb de javascript.
- [Entrevista a Chelsea Manning sobre IA](https://www.elsaltodiario.com/internet/entrevista-chelsea-manning-internet-inteligencia-artificial). Podes mirar su libro [README.TXT](https://readmetxt.xyz/)
- [Libro: Tecnotecas para la innovación popular argentina: Reconocimiento, formación y articulación productiva de los saberes tecnosociales de las juventudes](https://www.argentina.gob.ar/noticias/libro-tecnotecas-para-la-innovacion-popular-argentina-reconocimiento-formacion-y)

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [LA CRISIS DE LA NARRACIÓN - Byung-Chul Han](https://www.youtube.com/watch?v=p92UH6EZ5PM)
- [La importancia de la escritura manusrita](https://www.economist.com/culture/2023/09/14/the-importance-of-handwriting-is-becoming-better-understood)
- [El aire es una inmensa biblioteca, otra pedagogía](https://laescuela.art/es/campus/library/essays/el-aire-es-una-inmensa-biblioteca-otra-pedagogia-de-ivan-illich-por-guillermo-canek-garcia) - Sobre [Ivan Illich](https://es.wikipedia.org/wiki/Iv%C3%A1n_Illich)
- [Vivimos un fenómeno de demencia masiva](https://www.pagina12.com.ar/599173-vivimos-un-fenomeno-de-demencia-masiva)

### Tecnología
- [Configurar 2FA TOPT en keepassxc (authenticacion de dos pasos)](https://www.linux.org/threads/in-depth-tutorial-how-to-set-up-2fa-totp-with-keepassxc-aegis-and-authy.36577/)
- De las OWASP Cheat Sheet Series [Conseojs de seguridad sobre Django Rest Framework](https://cheatsheetseries.owasp.org/cheatsheets/Django_REST_Framework_Cheat_Sheet.html)
- El nuevo estandar Passkeys para autenticación en web. [Repo con ejemplo](https://github.com/ownid/passkeys) - [Web](https://www.passkeys.com)
- [Como compilar el kernel de Linux](https://itsfoss.com/compile-linux-kernel/)
- [Desarrollo Local-First](https://bricolage.io/some-notes-on-local-first-development/)

### LLMs
- [MAGE ✨ GPT Web App Generator](https://usemage.ai) -[Video Explicativo](https://www.youtube.com/watch?v=KQrGu8cnwvA)
- [Cómo hacer una injección de prompt?](https://simonwillison.net/2023/May/2/prompt-injection-explained/) - [Multi Modal](https://simonwillison.net/2023/Oct/14/multi-modal-prompt-injection/)
- [Usando AutoGen con cualquier "modelo Open-Source"! (RunPod + TextGen WebUI)](https://www.youtube.com/watch?v=FHXmiAvloUg)
    - [Autogen](https://github.com/microsoft/autogen) es un multi agente de IAs (interacción de diferentes IAs)
- [Zshot](https://github.com/IBM/zshot): Zero y/o Few shot con entidades nombradas y reconocimiento de relaciones
- [Mistral](https://mistral.ai/news/announcing-mistral-7b/): El mejor modelo 7B hasta ahora (Con licencia Apache 2)
