---
title: Mayo 2021
---

# Cambá CTTE Ezine Mayo 2021

**La espera**
![LaEspera](@img/laespera.jpg)

```
Cuando me olvido de ser,
cuando dejo de existir,
soy un cuerpo sin alma
voy vagando por ahi,
dentro de esta rutina,
que creo que no elegi.

¡Pero soy carne! ¡que quiere carne!
Soy vida que quiere vivir.
¡Pero soy carne! ¡que quiere carne!
Soy vida que quiere vivir.

Disfrutando mi prisión,
en vano es querer salir
¡Las entrañas me engañan!
Voy volando en la ciudad,
salpicando libertad
siento temor y vuelvo.
```

Aquí la letra de [Eruca Sativa](https://es.wikipedia.org/wiki/Eruca_Sativa) en el tema ["La carne"](https://www.youtube.com/watch?v=Z3gvULD4apI)


## Contribuciones al Software Libre
- [Coin Portfolio](https://github.com/glmaljkovich/coin_portfolio) - Una PWA para hacer el seguimiento de los gastos y ganancias con criptomonedas - *Gabriel Maljkovich*

## Herramientas de software

- **Seguridad de Ethereum**
  - [Buscar y extraer archivos blob de Ethereum](https://github.com/litneet64/etherblob-explorer)
  - [Módulo Ghidra para hacer decompilar smart contracts](https://github.com/adelapie/ghidra-evm)
  - [Preguntas frecuentes de Contract Library](https://www.contract-library.com/faq)
  - ¿Alguien probo algún decompilador de software libre: ABI -> Solidity? Acá algunos no libres [Porosity](https://github.com/comaeio/porosity), [EVM - Ethereum Contract Decompiler de JEB](https://www.pnfsoftware.com/jeb2/evm)
- [Yalc: utiliza paquetes yarn/npm packages localmente](https://github.com/wclr/yalc)
- [Biblioteca Arduino para controladora de discos floppy](https://github.com/dhansel/ArduinoFDC)
- [Transmití datos a través de parlates de PC](https://github.com/ggerganov/ggwave/tree/master/examples/r2t2)
- [Distribución Linux para teléfonos móviles](https://postmarketos.org/)

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [¡Wikipedia en español cumple 20 años!](https://www.wikimedia.org.ar/Wikipedia20/)
- [Participa de la encuesta de anual StackOverflow 2021 para programadorxs](https://stackoverflow.az1.qualtrics.com/jfe/form/SV_7arimtzVFf8ipfM)
- Entrevista a Linus Torvalds - [Primer parte](https://www.tag1consulting.com/blog/interview-linus-torvalds-linux-and-git), [Segunda parte](https://www.tag1consulting.com/blog/interview-linus-torvalds-open-source-and-beyond-part-2)
- Evento: [1st International Coop Community Meeting!](https://communitybridge.com/bbb-room/global-tech-coops-community/) - **Viernes 4 de Junio de 2012 a las 15:00 UTC** - *Code: techCoop*
- Evento: [La historia que no nos contaron: una cronología de la exclusión en sistemas](https://zoom.us/j/97329264356?pwd=bUpvQk9lQTAyK1hGM1Q1eTQrZ29tZz09) - **Jueves 3 de Junio de 2021 a las  17 hs (UTC-3)** - *Responsables:* Maia ([Eryx](https://eryx.co/)) , Flor ([Cambá](https://camba.coop)) y  Romi ([Nayra](https://nayra.coop/))


## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Lanzamiento de la página web del Archivo de la Memoria Trans Argentina](https://www.youtube.com/watch?v=PbZ_BDBO9ak) - [Web](https://archivotrans.ar/)
- Texto sobre [Varones y masculinidades - Herramientas pedagógicas para facilitar talleres con adolescenter y jóvenes](https://argentina.unfpa.org/sites/default/files/pub-pdf/Varones%20y%20Masculinidades.pdf) del  [*Instituto de masculinidades y cambio social*](http://institutomascs.com.ar/)
- [INAES entregó matricula a ALT Cooperativa](https://www.argentina.gob.ar/noticias/el-inaes-entrego-la-matricula-la-cooperativa-alternativa-laboral-trans) - [Web](https://altcooperativa.com/)

### Sociales/filosóficos

- Contribución que hacen socias de Cambá al texto *La economía popular ante la crisis* - [Educación para nuevas relaciones laborales: Cooperativas de trabajo tecnológicas y universidad](http://observatorioess.org.ar/wp-content/uploads/2020/10/Eje-2-completo-_30.pdf)
- [Ética de la riqueza - Uniendo Utopías](https://blog.diegosaravia.com/etica-de-la-riqueza-uniendo-utopias-53e601d0555e) - Diego Saravia
- [Degustación de Libros de Contracultura: Expreso Imaginario](https://www.youtube.com/watch?v=-ZdNxtb3Kjk)

### Tecnología

- Construí tu [Lámpara Cubo de Rubik](https://www.youtube.com/watch?v=wnBRsU6DJKY) (Fuentes código, Diagrama, Prototipo 3d, Fuentes scad)
- Todas las melodías musicales creadas: [Web](http://allthemusic.info/), [Generador Midi](https://github.com/allthemusicllc/atm-cli), [Dataset](https://archive.org/download/allthemusicllc-datasets)
- [¿Cómo evitar usar IF en js?](https://dev.to/damxipo/avoid-use-if-on-our-js-scripts-1b95)
- [Facebook dice que quiere llevar a Rust al mainstream](https://www.zdnet.com/article/rust-programming-language-we-want-to-take-it-into-the-mainstream-says-facebook/)

### Otras

Neto como actor Protagonista de Herramientas para la economía popular y solidaria (4 capitulos) - [Ver acá](https://www.youtube.com/playlist?list=PLeRiuwi5CR0NDsisO5Mv17qnEMbzxcCTu). Trabajo Intercooperativo de  [El Maizal](https://cooperativaelmaizal.com.ar/]), [Más Ruido](https://www.facebook.com/masruidocinedigital/), [Cooperativa de Diseño](https://cooperativadedisenio.com/) y [Cambá](https://camba.coop)
