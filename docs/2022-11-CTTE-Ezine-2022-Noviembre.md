---
title: Noviembre 2022
---

# Cambá CTTE Ezine Noviembre 2022

<div style="text- align: center;">

![Niña](@img/ninia.gif)

</div>

```
En el mundo hay una isla verde en la que vive sola una vaca.

Hasta que cae la noche, se alimenta de la rica vegetación que allí crece, de
manera que se pone grande y gorda. Pero, durante la noche, se queda más flaca
que un alambre a causa de su inquietud, pues se pregunta sin parar: «¿Qué
voy a comer mañana?».

Cuando rompe el día, los campos verdean: las hojas verdes y los cereales
alcanzan la altura de un hombre.

La vaca se echa encima con hambre canina; hasta la noche, se alimenta de
aquella vegetación y la devora por completo.

De nuevo se pone corpulenta, gorda y fuerte.

Luego, llegada la noche, es víctima del pánico y presa de una febril inquietud,
de suerte que, por miedo a no tener forraje, enflaquece pensando: «¿ Qué voy
a tener mañana para comer?».

Así se comporta aquella vaca desde hace muchos años.

Nunca se dice: «Durante todo este tiempo, me he alimentado de este prado y de
este pasto; mi subsistencia no me ha faltado un sólo día; ¿a qué, pues, este
temor y esta angustia que roe queman las entrañas?», Pues no, cuando cae la
noche, la vaca gorda se vuelve flaca pensando: «¡Ay! ¡ya no tengo nada para
comer!».

La vaca es el alma carnal, y el campo es el mundo en el que el alma carnal se
carcome de miedo por el pan cotidiano, diciéndose: «Me pregunto qué voy a comer
en el futuro: ¿dónde encontraré alimento mañana?».

Durante años has comido, nunca has estado privado de alimento: deja tranquilo
el futuro, considera el pasado.

Acuérdate de lo que has tenido ya; no pienses en lo que va a ocurrir, y no te
aflijas.
```

Poema [Masnaví](https://en.wikipedia.org/wiki/Masnavi) **"La vaca en la isla verde"** escrito por [Yalal ad-Din Muhammad Rumi](https://es.wikipedia.org/wiki/Yalal_ad-Din_Muhammad_Rumi), tomado del libro **75 Cuento Sufies** compilado por [EVA DE VITRAY MEYEROVITCH](https://en.wikipedia.org/wiki/Eva_de_Vitray-Meyerovitch).


## Herramientas de software

- [Snikket](https://snikket.org/) - Chat simple, seguro y privado con apliación y servidor
- [Penpot](https://github.com/penpot/penpot) - Plataform de diseño y prototipedo usando estandares
- [Notesnook](https://github.com/streetwriters/notesnook) - Tomando notas encriptadas.
- [Nyxt](https://github.com/atlas-engineer/nyxt) - El power-browser para hackers
- [Claper](https://github.com/ClaperCo/Claper) - Herramienta para interactuar con la audiencia

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [FemITConf 2022](https://lasdesistemas.org/femitconf2022/) - Sábado 12 de noviembre de 2022 en línea. [Registrate](https://www.eventbrite.com.ar/e/femit-conf-22-tickets-444569317797)
    - Con homenaje incluido a Ángeles Tella Arena de la UNQ a las 11:30hs
- [Webalmanac - Informe sobre el estado de la web](https://almanac.httparchive.org/en/2022/)
- [¿Es node más rápido que otros lenguajes de programación?](https://www.quora.com/Is-Node-js-faster-than-using-other-competitive-solutions-in-the-market) - [Fuente de datos](https://www.techempower.com/benchmarks/#section=data-r21)
- [IEEE - Ranking de lenguajes de programación 2022](https://spectrum.ieee.org/top-programming-languages-2022)
- [Proyecto diagnóstico y mitigación de sesgos desde América Latina](https://www.vialibre.org.ar/proyecto/proyecto-diagnostico-y-mitigacion-de-sesgos-desde-america-latina/)

## Noticias, Enlaces y Textos

### Tecnología

- Kernel Linux - [Linus Torvalds, Creator of Linux & Git, in conversation with Dirk Hohndel](https://www.youtube.com/watch?v=sLimmpZWRNI) y [Greg Kroah-Hartman en conversación con Hernán Gonzáles de GAIA](https://www.youtube.com/watch?v=dZ_B5Jke5L4)
- [Como crear un paquete python en 2022](https://mathspp.com/blog/how-to-create-a-python-package-in-2022)
- Tutoriales de juegos con REACT-WASM-RUST: [Snake](https://www.youtube.com/watch?v=iR7Q_6quwSI), [Mindsweeper](https://www.youtube.com/watch?v=0ywizYLPV00) y [Tetris](https://www.youtube.com/watch?v=_lAr7JveRVE)
- [Progrmando un Hola Mundo para EFI](https://www.rodsbooks.com/efi-programming/hello.html)
- [Resultados de js13kGames](https://github.blog/2022-10-06-js13k-2022-winners/) - Competencia de creación de juegos en Javascript de menos de 13kb

### Cooperativas/Economía Popular

- [¿Puede la economía del conocimiento ser cooperativa? - Hablando con Neto Licursi presidente de FACTTIC](https://www.lalunacongatillo.com/puede-la-economia-del-conocimiento-ser-cooperativa/)
- [Podcast de FACTTIC -  Desmuteando: Episodio 2 - Bantics](https://www.youtube.com/watch?v=5yzJGkNj1x4)
- [Podcast de FACTTIC -  Desmuteando: Episodio 3 - Eryx](https://www.youtube.com/watch?v=It3oOICHNR0)
- [Como migrar 6300 hosts a gnu/linux usando Ansible y AWX](https://osiux.com/2022-10-20-howto-migrate-6300-hosts-to-gnu-linux-using-ansible-and-awx.html)
