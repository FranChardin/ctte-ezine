---
title: Septiembre 2021
---

# Cambá CTTE Ezine Septiembre 2021

**Buscando el sol**
![buscandoelsol](@img/buscandoelsol.png)

```
Hay un lugar dónde nunca
pasa lo que no querés
y ese lugar no le teme
a lo que vos le temés

Suenan canciones de fama mundial
de esas que no conocés
siempre está ahí para hacerte acordar lo que hacemos
y no te pide más que un sólo beso a la vez

Hay quién se pinta la cara
y otros pintados están
no se presenta la suerte
y ese lugar se te va

No corras más que ya no hay dónde huír
de esa piel no te escapás
y hasta la muerte se ríe de tantos milagros
y hasta que algún sabor agrio te obligue a pensar...

Por qué será por qué me tocó a mí
será surgir o debo desistir
ante la oscura y siniestra posibilidad
de no llegar a dónde debo ir

Quizá perder a veces mi lugar
quizá surgir de un "ya no puedo más"
y me convenza para siempre de mi condición
de boca libre y pies pa' caminar

A desalambrar, a desalambrar...
```

Aquí la letra de [Alejandro Balbis](https://es.wikipedia.org/wiki/Alejandro_Balbis) en el tema ["Mi Lugar"](https://www.youtube.com/watch?v=Nhc6WnR0tEM)


## Herramientas de software

- [Ayni](https://github.com/gcoop-libre/ayni) - Juego Educativo sobre cooperativismo
- [Kontra](https://straker.github.io/kontra/) - Micro Biblioteca para creación de juegos en js
- [MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF) - Sistema automatizado para analisis de seguridad de aplicaciones móviles.
- [CamPhis](https://github.com/techchipnet/CamPhish) - Tomar fotos de dispositivo accediendo a web
- [Solitude](https://github.com/nccgroup/solitude) - Herramienta de análisis para hacer investigaciones sobre privacidad

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Participar en Entrevista a desarrollarxs en django](https://surveys.jetbrains.com/s3/w-django-developers-survey-2021)
- [Presentación de conclusiones o datos de Encuesta de StackOverflow](https://insights.stackoverflow.com/survey/2021)
- 30 años de linux [1](https://www.lanacion.com.ar/tecnologia/linux-cumple-30-anos-el-miercoles-nid21082021) [2](https://www.msn.com/en-us/news/technology/linux-turns-30-%E2%80%8Blinus-torvalds-on-his-just-a-hobby-operating-system/ar-AANK0Cj)
- [Nueva versión 11 de Debian llamado "Bulleye"](https://www.debian.org/download) (luego de 2 años, 1 mes y 9 días de trabajo)
- [FlashParty del 3 al 5 de septiembre: Evento Online](https://flashparty.rebelion.digital/index.php?lang=es)


## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [¿Quién le debe a quién?](https://tintalimon.com.ar/libro/qui%C3%A9n-le-debe-a-qui%C3%A9n/) - [1](https://www.youtube.com/watch?v=ylbdruBbp4Y)
- [La HISTORIA del ANARQUISMO en 10 minutos | ¿Qué es el anarquismo?](https://www.youtube.com/watch?v=6kMVCsx9tG4)
    - Probar el paquete `sudo apt install anarchism` y luego pone en el navegador  `file:///usr/share/doc/anarchism/html/index.html`
- [Los 4 principios para construir DISCIPLINA según Marco Aurelio y Séneca | Estoicismo paso a paso](https://www.youtube.com/watch?v=OjOymTNuE3Q)
- [“Ahora uno se explota a sí mismo y cree que está realizándose”](https://elpais.com/cultura/2018/02/07/actualidad/1517989873_086219.html)
- [La gente feliz no necesita consumir](https://www.elclubdeloslibrosperdidos.org/2018/04/la-gente-feliz-no-necesita-consumir-la.html)


### Tecnología

- [Testeando aplicaciones React Native Apps](https://semaphoreci.com/blog/testing-react-native-apps)
- [Material para aprendizaje de Blockhchain por Protofire](https://github.com/protofire/blockchain-learning-path)
- [Blockchain: game changer vs. hype](https://stackoverflow.blog/2021/06/07/most-developers-believe-blockchain-technology-is-a-game-changer-3/)
- [El libro del conocimiento secreto](https://github.com/trimstray/the-book-of-secret-knowledge)

### Otras

- [Reporte de como se usan internet y redes en Cuba](http://humanidades.com.ar/comunidadesv/explorando-el-internet-alternativo-y-formas-inusuales-de-networking-en-la-habana)
- [Parte de Documental de DRECERES 1 sobre Calafou](https://fediverse.tv/videos/watch/6286a98a-4b48-4662-bac4-28c6fbbde035)
- Redes Comunitarias financiadas con dinero del Fondo de Servicio Universal a través de los nuevos programas del ENACOM. [Post y video](https://fb.watch/7c9JGUT8C4/)
