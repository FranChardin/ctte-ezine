# Hacelo vos misma

> Extracto de página 295 y 296 de [Ninguna línea recta, contraculturas punk y politicas sexuales en argentina (1984-2007)](https://www.trenenmovimiento.com.ar/nlr/nlr.html) - Nicolas cuello y Lucas disalvo

La noción de hacelo vos misma, es este sentido, postula que el aprendizaje y
la creación, la teoría y la práctica, lejos de ser tiempos diferenciados en
donde primero se aprende a hacer yluego se hace, son parte de una misma
sensibilidad generadora que se conjura a través del ensayo: se aprende haciendo
y haciendo se produce conocimiento. El hacelo vos misma sostiene a la práctica
como un ejercicio activo de formas radicales de autonomía suceptibles de
reestructurar vínuclos, dinámicas, sentidos y maneras existentes de afectarse
entre los sujetos, las cosas y el mundo. Stephen Ducombe percibe la conexión
histórica entre el gesto crucial punk de hacelo vos misma con los modos del
amateur y postula que "en una sociedad que honra al profesionalismo y al valor
del dólar, las raíces del amateurismo son mucho más nobles", reparando de esta
manera en su base etimológica: "amator, latín para amante". Del mismo modo, al
indagar en la trayectoria histórica de la palabra amateur, Vera Keller se
encuentra con un poderoso entrelazado de las dimensiones del sentir, el amar,
el hacer, dado que, después de todo amante puede pensarse como alguien que
hace de su amor, de su fuerte conexión con lo sensible con algo o alguien,
una práctica vital sostenida. El hacer amateur es fuertemente despreciado por
las formas sobrias y elitistas de la experticia y la calificación, porque
de alguna manera introduce en la práctica a un cuerpo arrebatado de emoción
u afectos indisciplinados. Este hacer amateur, este hacer amante, no es el
hacer profesional, recto y descorporizado que se le atribuye presuntamente a
la razón modernam sino que es un hacer ardoroso, corpóreo y errático movido
por un deso hipervincular, es el hacer de las maquinas deseantes que renuevan
en cada movimiento su indevlinable pasión por la posibilidad. No necesariamente
este efecto amateur se expresa como una pasión indulgente y pacífica: Ducombe
describe este registro sentimental urgente y desordenado que caracteriza al
origen y a la lengua de los fanzines, al sostener que éstos son hijos del amor
y la rabia creadora.
