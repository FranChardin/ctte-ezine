---
title: Junio 2021
---

# Cambá CTTE Ezine Junio 2021

**Invierno**
![Invierno](@img/invierno.jpg)

```
Si no te gusta lo que ves al despertar
Si el día no tuvo un sentido, ni una victoria más
Si nadie esta contigo en esta lucha a librar

Aunque no veas otra salida
Cuando la niegan no hace más que afirmar
Aun queda tiempo para cambiar
Es el momento de volver

La impotencia va ganando su lugar
Te escucho quejarte pero no te veo actuar
si te incomoda lo que vez
empezalo a cambiar, que aun queda tiempo

Aunque no veas otra salida
Cuando la  niegan no hace más que afirmar
Aun queda tiempo para cambiar
Es el momento de volver

Recapitula, detente
pensá lo que queres
aun falta muchos caminos por cambiar

Aunque no veas otra salida
Cuando nada más que afirmar
Aun queda tiempo para cambiar
Es el momento de volver

Y mientras espero
Más y más desespero
```

Aquí la letra de [Madilon](https://www.facebook.com/Madilon-Argentina-FCO-133724500053283/) en el tema ["Cambios"](https://www.youtube.com/watch?v=1QWvuVqInIk)


## Herramientas de software

- [pyWhat](https://github.com/bee-san/pyWhat) identifica de manera simple emails, direcciones de IP, etc.
- [Deskreen](https://github.com/pavlobu/deskreen) convierte cualquier dispositivo con un navegador como pantalla secundaria de tu computadora.
- [PyCryptoBot](https://github.com/whittlem/pycryptobot) es un Bot para compra/venta de crypto monedas, [más info](https://medium.com/coinmonks/python-crypto-bot-pycryptobot-b54f4b3dbb75)
- [Ghost](https://github.com/TryGhost/Ghost) - Manejador de contenidos headless en Node.js para publicaciones profesionales


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Firmina: Google anuncia un cable sub marino de USA a Argentina](https://techcrunch.com/2021/06/09/google-announces-the-firmina-subsea-cable-between-the-u-s-to-argentina/)
- [Libro para entrevistas de Machine Learning](https://huyenchip.com/ml-interviews-book/)
- [Peliculas curadas para hackers y cyberpunks](https://hackermovie.club/)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Contra la captura heteromoral capacitista de la responsabilidad afectiva y el poliamor](https://www.youtube.com/watch?v=WHKSn-OZRr4)
- [GOZA](https://futurock.fm/goza/) es un sello discográfico que nace con el compromiso de apostar a la igualdad.
- [Spideralex - Por debajo y por los lados. Sostener infraestructura feminista con ficción especulativa](https://iterations.space/files/iterations-publication/iterations-spideralex.pdf)


### Sociales/filosóficos

- Debate de Chomsky y Foucault sobre la Naturaleza Humana, Justicia versus poder: [Video](https://www.youtube.com/watch?v=3wfNl2L0Gf8) - [Anotaciones de Livchits Leonel](https://www.redalyc.org/pdf/282/28291817.pdf)
- [Más bueno que un hacker](https://revistacrisis.com.ar/notas/mas-bueno-que-un-hacker) - Cuatro casos ocurridos recientemente en la Argentina revelan la fragilidad del sistema y cómo son los hackers quienes pagan las consecuencias por denunciarlo.
- [El banquero anarquista: una sátira cualquiera, desgranamos la primera y única de las "sátiras dialécticas" de Fernando Pessoa](https://www.eldiario.es/cantabria/amberes/banquero-anarquista-satira_132_3429949.html)

### Tecnología

- Creación de redes libres en Argentina: [Video](https://www.youtube.com/watch?v=gQY0Pp5iYlk), Organizaciones: [Altermundi](https://altermundi.net/), [Atalaya sur](http://www.villa20.org.ar/atalaya-sur-llevar-internet-desde-lugano-jujuy/)
- [Construcción de la Web Auto Alojada de bajo consumo](https://wiki.calafou.org/index.php/Una_pagina_web_auto-alojada_de_bajo_consumo) de la [Calafou - colonia ecoindustrial postcapitalista](https://calafou.org)
- [Construcción de “Armachat” intercomunicador para redes LoRa](https://makezine.com/projects/armachat-lora-communicator)

### Otras

- [Hablemos de LibreOffice - Desarrollo de la Herramienta IA2](https://fediverse.tv/videos/watch/1736c1a4-4ea8-422f-ac15-8090c196a20b)
- [Otra forma de desarrollo tecnológico es posible](http://anccom.sociales.uba.ar/2021/06/28/otra-forma-de-desarrollo-tecnologico-es-posible/)
- La Universidad Nacional de Quilmes Reconoce y avala el uso del lenguaje inclusivo en cualquiera de sus modalidades como recurso válido en las producciones académicas realizadas por las personas integrantes de la comunidad universitaria ([Resolución](https://drive.google.com/file/d/1z6YbrRYL6LviuCPVCkitSGzfevdkrf4Z/view?usp=sharing), se están modificando los programas de las materias acordes a esta última resolución.

