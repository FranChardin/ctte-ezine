# El aborto ilegal asesina mi libertad

Una de las primeras veces que escuche hablar sobre aborto en mi adolescencia fue cuando compre durante 1997 un Disco 7" Split de [Fun People](https://es.wikipedia.org/wiki/Fun_People) y [She Devils](https://youtu.be/O3DAm82vIvU), llamado **El aborto ilegal asesina mi libertad**.

![Mi Disco](@img/7-shedevils.jpg)


## Escuchando

Mientas escucho [este mismo disco](https://www.youtube.com/watch?v=zrz_KkfALh4), transcribo algunos textos que integran un librito al interior del disco.

### Introducción

> El aborto ilegal puede ocasionar hasta 150000 muertes por año. Mientras que el aborto se practica en condiciones legales y reglamentarias es relativamente seguro. El aborto es legal o ilegal con restricciones en 145 países e ilegal o permitido unicamente para salvar la vida de la madre en 36 países. Salvo en Cubo en toda Latinoamérica el aborto no esta permitido.

### Nuestro cuerpo nuestra desición por las despenalización del aborto ya!! ¿Por que?

- Muchas mujeres quedan embaradas sin desearlo y quiern tener un aborto
- El aborto practicado a mujeres en buen estado de salud es en general una operación sencilla que se vuelve riesgosa al ser clandestina y en este caso no esta sometida a un control médico o asepcia alguna más allá de la responsabilidad y conciencia de aquellos que la practican quieran tomar. El aborto da lugar a practicas médicas inadecuadas y así, cualquier posible malapraxis no es factible de ser juzgada. El mercado negro en torno al aborto posibilita la existencia de una variedad de prácicas médicas y no médica que especulan con la urgencia y desesperación de la mujer.
- El aborto será más o menos peligroso, esto depende de las posibilidades económicas de cada mujer y eso determinara su acceso a una clínica, consultorio, o casa particular.
- Los abortos clandestinos ponen en riesg la integridad física y mental de las mujeres.
- El aborto ilegal es una de las principales causas de mortalidad femenina (paros cardiacos, infecciones) y quienes mueren por falta de higiene y seguridad en una gran mayoría son pobres.
- La ilegalidad del aborto promueve el temor y la culpabilidad y en muchos casos las mujeres que no pueden afrontarlo, tienen hijos no deseados y viven una maternidad forzada.
- La principal razón es porque **MI CUERPO ES MIO Y ME DESICION TAMBIEN**

### ¿Porque el aborto es penalizado en Argentina?

- La sacralización de las maternidad.
- Porque al ser considerado una aberración por la iglesia católica, es una batalla donde el poder eclesiástico difícilmente ceda en su idelogía.
- La iglesia católica considera al aborto como una aberración. Difcilmente cedan en su posición a favor de una legislación que controle, para qu e haya menors muertes de mujeres o mujeres más saludables. El poder eclesiástico influye contundentemente en las decisiones del gobierno
- El poder político y los que legislan son en su amplia mayoría hpombre y si bien en su momento se han encargado de luchar con la iglesia por la declaración del divorcio legal (algo a lo que la iglesia católica se oponía fuertemente) ¿Porque no se libra una discusión con el mismo ímpetu sobre le tema del aborto? Es tal vez porque las mujeres son consideradas todavía una minoria y sufren la desigualdad de género.
- No es una cuestión moral, ni ideológica, es un asunto de salud y derecho.
- No hay presión social que demande una solución a este problema, que involucra mucha hipocresía.
- La ineficiencia de los anticonceptivos o la falta de uso de los mismo y las relaciones sexuales forzadas llevan a que muchas mujeres queden embarazadas sin desearlo. El derecho al aborto se fundamenta en el derecho de la mujer a controlar su cuerpo, reproducción y los valores básicos de la libertad a la autodeterminación y la igualdad.
- En Argentina: Hay datos que mencionan a cientos de miles de abortos clandestinos al año. Entre los 20 y 30 años, 400 mujeres mueren cada año a causa de abortos ilegales.
- Les recordamos que las complicaciones más frecuentes son:
  - Perforación del úter
  - Lesión del intestino
  - Insuficiencia renal
  - Cuadros de grangrena que terminan en la extirpación del útero.
  - Lesiones en las trompas que disminuyen la capacidad reproductiva
- En argentina el aborto es legal únicamente para proteger la salud de la madre, defecto fetales serios o en casos de violación. ( Si la mujer es Canela y el violador GG Allin...)
- Anticoncepción: los métodos son variados y debes adaptarte al que más te plazca (pildora, dui, diafragma, espermicidas, ligaduras de trompas, vasectomia).


### Grafiti

> Creo que la despenalización del aborto evitaría que muchas chicas murieran en clinikas por praktikas ilegales. Respetemos a la mujer y su cuerpo por la vida. Aunque nuestras opinines son poco escuchadas el problema es real. Abrán sus ojos, ellas también sufre.


## Sigo escuchando

Escuchando el mismo disco, [una y otra vez](https://www.youtube.com/watch?v=zrz_KkfALh4) y transcribo las letras de las canciones.

En voces de [Patricia ](https://es.wikipedia.org/wiki/Patricia_Pietrafesa) y [Carlos](https://es.wikipedia.org/wiki/Carlos_Rodr%C3%ADguez_(m%C3%BAsico)) escucho:

### She devils - Nada para mi

```
Reclamo perdida toda esperanza
a quien le puede importar?
decís ke todo ya esta hecho
y no hacés nada más
seguro queres gritar
seguro queres cambiar

si no te apuras un poco ya no va a importar...

nada para mi, sim embargo se!
nada para mi, sim embargo se!
lo sé...sé bien

Tengo muy pocas cosas claras
y mucho de que dudar
el espacio, el tiempo
la dimensión
el devenir y el porvenir
el futuro decir, el presente no es aki
el ser no ser el, el yo no yo
son nada para mi

nada para mi, sim embargo se!
nada para mi, sim embargo se!
lo sé...sé bien
```

### She devils - Baby

```
Siempre pensas que te equivocas,
¿Por qué no confiás?
Nadie parace saber como actuar.
Nosotros queremos: escapar, escapar!!!!

Ey, babe ¿qué vas a hacer? ¿Quedarte donde esperan que estes?
Haciendo lo que se espera que hagas.

De crisis en crisis, de bar en bar.
¿Qué te quedas a esperar?
Nadie parece saber como actuar
Nena tenés que: defender tu paz, defender tu paz!!!!

Ey, babe ¿qué vas a hacer? ¿Quedarte donde esperan que estes?
Haciendo lo que se espera que hagas.
```

### Fun people - Lady

```
honey, honey, honey
Yo lo se, no es tan facil
Lady! no estas preparada,
no tienes el dinero
no estas sana para tener un bebe
vos no querías abortar,
pero no tenias opción!

You did take care, you did take care,
but it didn't work!
Te cuidaste, te cuidaste,
pero eso no funciono
You did take care, you did take care,
but it didn't work!
Te cuidaste, te cuidaste,
pero eso no funciono

Lady, vos no sos una mala mujer, no
por haber quedado embarazada
y haber pensado en abortar,
vos no sos la culpable vos no tenías opcion!

You did take care, you did take care,
but it didn't work!
Te cuidaste, te cuidaste,
pero eso no funciono
You did take care, you did take care,
but it didn't work!
Te cuidaste, te cuidaste,
pero eso no funciono

te vas a quedar asi?

Si tuvieran en cuenta nuestros reclamos
las cosas seran muy distinas
sabias? Lo sabes?
honey, no llores... Pelea!
```

### Fun people - Valor interior

```
Sabes el esfuerzo que tube que hace
para poder comer
y mentalizarme que mi cuerpo estaba bien
Crei que al llegar a estar en linea todo iba a
cambiar para bien.
Y Asi pasa el tiempo y tu juventud,
no comiendo y pensando nada más que en eso
Glorificandola corteza e ignorando lo invisible
tu valor interior, tu valor interior.
```
