---
title: Junio 2023
---

# Cambá CTTE Ezine Junio 2023

<div style="text- align: center;">

![Taza](@img/junio2023.jpg)

</div>

```
No tengo hambre
Tengo ansiedad
Ver tanta gente acá reunida
Me dan ganas de fumar

Lo que pasa es que en casa
Muy solo se está
Ahí va otro inadaptado
Intentando encajar

No sé muy bien en dónde
Encajar, no sé ni para qué

Si no hay botón de pausa
No hay rebobinar
Por eso es que no me pierdo
Ningún evento social

Hace tiempo estoy pensando
Tengo que parar
Mientras tanto por las dudas
Sigo a toda velocidad

Tírame un fósforo y me prendo fuego
Pam pararam pararam
Tírame un fósforo si no me muero

Ya no tengo uñas para masticar
Solo este par de ojos que no paran, no paran, no paran de mirar
A la gente con sus ganas de justificar
Con urgencia su existencia ante todos los demás

Tírame un fósforo y me prendo fuego
Pam pararam pararam
Tírame un fósforo si no me muero

No sos
Tan especial como te dijo mamá
Está en el gris tu mejor matiz

Ya sé
Que no es tan fácil como parecía
Pero hasta un reloj roto
Da la hora dos veces al día

Tírame un fósforo y me prendo fuego
Pam pararam pararam
Tírame un fósforo si no me muero

Cundirá el pánico
Cundirá el pánico
Cundirá el pánico
Voy a estar bien

Cundirá el pánico
Cundirá el pánico
Cundirá el pánico
Voy a estar bien

Está en el gris
Tu mejor matiz
```

Letra de la canción [No tengo hambre, tengo ansiedad](https://www.youtube.com/watch?v=P3AUJSDnLnA) de [Alan Sutton y las criaturas de la ansiedad](https://rock.com.ar/artistas/alan-sutton-y-las-criaturitas-de-la-ansiedad/)

## Herramientas de software
- [Jam](https://gitlab.com/jam-systems/jam): Alternativa libre a Clubhouse, Twitter Spaces y otros simaudio spaces similares
- [HiddenWave](https://github.com/techchipnet/HiddenWave): Esconde tus secretos en una archivo Wav
- [Tool-x](https://github.com/ekadanuarta/Tool-X): Tool-X es la herramienta de Kali para Termux
- [Tema Dracula](https://draculatheme.com/): Temas oscuros para muchas aplicaciones diferentes
- [Carbonyl](https://github.com/fathyb/carbonyl): Chromium dentro de tu terminal


## Informes/Encuestas/Lanzamientos/Capacitaciones
- [OhMyGit](https://ohmygit.org/): Juego para aprender GIT
- [Lanzamiento de MOJO](https://www.fast.ai/posts/2023-05-03-mojo-launch.html): ¿el lenguaje de programación con más avance en decadas?
- [Computadoras Starlab](https://ar.starlabs.systems/)
- [CommandLineFu](https://www.commandlinefu.com/): Lugar que contiene un registro de gemas de la línea de comandos.


## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Pepe Mujica y Juan Grabois](https://www.youtube.com/watch?v=N-4liql7eNg): Ética de la austeridad e integración latinoamericana
- [Proyecto Ballena 2023 | Mesa debate | Manes y Grabois](https://www.youtube.com/watch?v=qyNIoZY7uyI)
- [Artificial Idiocy - Slavoj ZIZEK](https://www.bloghemia.com/2023/05/sobre-la-inteligencia-artificial-por.html)
- [Anti-corporación](https://www.youtube.com/watch?v=0zMFbrMbAPc&t=1050s) [1 minuto]
- [Conversación infinita de Slavoj Zizek y Werner Herzog](https://infiniteconversation.com/)


### LLM
- [OpenLLaMA](https://github.com/openlm-research/open_llama): Reproducción abierta de LLaMa
- [RedPajama](https://github.com/togethercomputer/RedPajama-Data): Los datos RedPajama contiene codigo para preparar datasets para entrenar LLMs
- [LocalAI](https://github.com/go-skynet/LocalAI): API local manejada por la comunidad compatible con OpenAI
- ChatGPT en la terminal [Shell Genie](https://github.com/dylanjcastillo/shell-genie), [tgpt](https://github.com/aandrew-me/tgpt)
- [MPT-7B](https://www.mosaicml.com/blog/mpt-7b): Nuevo estandar LLM para Open-Source comercialmente utilizable.
- [Stable diffusion Webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)


### Tecnología
- [Can Rust Beat Javascript in 2023?](https://joshmo.bearblog.dev/can-rust-beat-javascript-in-2023/)
- [¿Alguien tiene mis contraseñas?](https://argentina.mefiltraron.com/)
- [Usando un pager en el siglo 21](https://debugger.medium.com/howto-using-a-pager-in-the-21st-century-6a57454ecde8)
- [Usando Rust in microcontroladores](https://zoe.kittycat.homes/log/rust_atmega32u4_tutorial)
