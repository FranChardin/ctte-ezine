---
title: Julio 2022
---

# Cambá CTTE Ezine Julio 2022

<div style="text-align: center;">

![Agujeronegro](@img/agujeronegro.png)

</div>

```
La madrugada estalla como una estatua
Como estatua de alas que se dispersan por la ciudad
Y el mediodía canta campana de agua
Campana de agua de oro que nos prohíbe la soledad
Y la noche levanta su copa larga
Su larga copa larga, luna temprana por sobre el mar

Pero para María no hay madrugada
Pero para María no hay mediodía
Pero para María ninguna luna
Alza su copa roja sobre las aguas

María no tiene tiempo (María Landó)
De alzar los ojos
María de alzar los ojos (María Landó)
Rotos de sueño
María rotos de sueño (María Landó)
De andar sufriendo
María de andar sufriendo (María Landó)
Sólo trabaja
María sólo trabaja, sólo trabaja, sólo trabaja
María sólo trabaja
Y su trabajo es ajeno
```

**María  Landó** es una canción cuya música compone **Chabuca Granda** y cuya letra es sacada de un poema de **César Calvo**, quien una noche en 1983, tras la muerte de Chabuca, se lo muestra a **Susana Baca**.
Acá interpretado por [Sin líneas en el mapa](https://www.youtube.com/watch?v=ZROiZxaLSfs).
¨

## Herramientas de software

- [Pyscrypt](https://pyscript.net/) - PyScript es un framework que permite crear aplicaciones python que funcionan en el navegador usando interfaz web. [Código fuente](https://github.com/pyscript/pyscript)
- [QuickJS](https://bellard.org/quickjs/) - Motor javascript minusculo
- [Signoz](https://github.com/signoz/signoz) - Aplicación software libre de Monitoreo de rendimiento (APM) y observabilidad (alternativa a Datagog, NewRelic, etc)
- [LiveTerm](https://github.com/Cveinnt/LiveTerm) - Crear tu sitio web con estilos de terminal muy rapidamente. [Ejemplo](https://liveterm.vercel.app/)
- [Nexe](https://github.com/nexe/nexe) - Crear ejecutabe desde un programa escrito en node.js

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Estado de wasm 2022](https://blog.scottlogic.com/2022/06/20/state-of-wasm-2022.html)
- [Que hacer felices a los programadores en el trabajo](https://stackoverflow.blog/2022/03/17/new-data-what-makes-developers-happy-at-work/)
- [Como deshabilitar el seguimiento de Ad Id en iOS y Android, y porque lo deberias hacer](https://www.eff.org/es/deeplinks/2022/05/how-disable-ad-id-tracking-ios-and-android-and-why-you-should-do-it-now)
- [Node.js Development Business Guide](https://www.ideamotive.co/node/guide)
- [Nvidia da un paso en relacion a la liberación de sus drivers](https://arstechnica.com/gadgets/2022/05/nvidia-takes-first-step-toward-open-source-linux-gpu-drivers) - [Código fuente](https://github.com/NVIDIA/open-gpu-kernel-modules)


## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Prefacio "El tercer Inconsciente" Franco Bifo Berardi](CTTE-texto-3er-inconsciente.md) - La psicoesfera en la época viral,  [Libro](https://cajanegraeditora.com.ar/libros/el-tercer-inconsciente/)
- Debate de ideas Milei-Grabois con Fontevecchia, [Parte 1](https://www.youtube.com/watch?v=CeTSfOcryD4) - [Parte 2](https://www.youtube.com/watch?v=FjEdHdQfl0E)
- [Audio Libro El patriarcado del salario](https://www.youtube.com/watch?v=xVR7IU_GKxs) - [Silvia Federici](https://es.wikipedia.org/wiki/Silvia_Federici)
- [Ochenta y dos consejos  para una para llevar una vida mental y físicamente sana, libre y responsable](https://pijamasurf.com/2014/07/82-consejos-de-gurdjieff-a-su-hija-una-poderosa-lista-de-sobria-sabiduria/) - del Libro "El maestro y las magas" de [Alejandro Jodorowsky](https://es.wikipedia.org/wiki/Alejandro_Jodorowsky), en donde el autor chileno atribuye la lista de consejos a [Gurdjieff](https://es.wikipedia.org/wiki/George_Gurdjieff).
- [Cómo ser anticapitalista en el Siglo XXI](https://www.youtube.com/watch?v=WN3XFNmfFug) - [Erik Olin Wright](https://www.ssc.wisc.edu/~wright/)


### Tecnología

- [Pagos mediante ultrasonido](https://dev.to/stripe/ultrasonic-payments-2958) - [quiet-js](https://github.com/quiet/quiet-js/)
- [Porque las criptomonedas deberian arder y morir](https://rebelion.digital/2022/06/05/porque-las-criptomonedas-deberian-arder-y-morir/)
- [Generar imagenes desde texto](https://wandb.ai/dalle-mini/dalle-mini/reports/DALL-E-mini-Generate-images-from-any-text-prompt--VmlldzoyMDE4NDAy) - [Dalle-mini](https://github.com/borisdayma/dalle-mini)
- [Objetos 3d con react three fiber](https://betterprogramming.pub/travel-in-and-out-of-3d-objects-using-react-three-fiber-36fa514ab6b5)
- [Ataques de phising de Browser en Browser](https://mrd0x.com/browser-in-the-browser-phishing-attack/)


### Cooperativas/Economía Popular
- [El espíritu viajero e itinerante del Laboratorio de Tecnologías Creativas](https://ltc.camba.coop/2022/06/22/volviendo-a-las-rutas/)
- [Charlie y Cecilia charlando de Coopcyle en Noticiero Pupilar de barricada TV](https://youtu.be/MvBhksSBB3U?t=2751)
- [Código de ética](https://coopersystem.com.br/wp-content/uploads/2021/05/codigo_etica_coopersystem.pdf) - [CooperSystem](https://coopersystem.com.br/) Cooperativa de Trabajo del área de tecnología de Brasilia, Brasil.


